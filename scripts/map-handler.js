// Jquery is known by Wordpress so ignore the warning that is is unknown.
// noinspection JSUnresolvedFunction
jQuery(document).ready(function ($) {
    let increaseHexBrightness = function (hex, percent) {
        // strip the leading # if it's there
        hex = hex.replace(/^\s*#|\s*$/g, '');

        // convert 3 char codes --> 6, e.g. `E0F` --> `EE00FF`
        if (hex.length === 3) {
            hex = hex.replace(/(.)/g, '$1$1');
        }

        let r = parseInt(hex.substr(0, 2), 16);
        let g = parseInt(hex.substr(2, 2), 16);
        let b = parseInt(hex.substr(4, 2), 16);

        return '#' +
            ((0 | (1 << 8) + r + (256 - r) * percent / 100).toString(16)).substr(1) +
            ((0 | (1 << 8) + g + (256 - g) * percent / 100).toString(16)).substr(1) +
            ((0 | (1 << 8) + b + (256 - b) * percent / 100).toString(16)).substr(1);
    }

    //Store the amount of maps that exist and have been initialized on this page.
    let currentMapId = 1;

    $('.managee-map.uninitialized').each(function () {
        $(this).removeClass('uninitialized');

        // Variables
        //The Leaflet Map
        let map;

        let mapId = $(this).attr("data-map-id");
        let mapName = $(this).attr("data-map-name");
        let tileUrl = $(this).attr("data-map-url");
        let mapHeight = $(this).attr("data-map-height");
        let fullscreen = $(this).attr("data-fullscreen") === "true";

        let measurementNodesById = {};
        let measurementNodesList = measurement_nodes;
        for(let i = 0; i < measurementNodesList.length; i++) {
            let node = measurementNodesList[i];
            measurementNodesById[node['id']] = node;
        }

        //Tile Layer
        let tileLayer;

        let selectedFeatureBorderColor = '#ffde2f';

        // noinspection JSUnresolvedVariable
        let isEditor = $(this).attr("data-editor") === "true";

        //DOM elements
        let rootContainer = $(this);
        let measurementNodeSelect, mapElement, mapMenu, infoElementLoader, detailSpan,detailButton, layerElement, mainContainer, infoElement, infoElementTitle, infoElementContent, editView, jsonView, settingsView, saveResponse, jsonContent, editTitle, editName, editLayer, editInfoContentButton, editTable, editColor, editIconMessage, editIconContainer, editIcon;

        //Markers
        let markerSize = 32;
        let idleIcon = L.divIcon({
            className: 'round-icon',
            iconSize: [markerSize, markerSize],
            iconAnchor: [markerSize / 2, markerSize / 2]
        });
        let selectedIcon = L.divIcon({
            className: 'round-icon selected',
            iconSize: [markerSize, markerSize],
            iconAnchor: [markerSize / 2, markerSize / 2]
        });

        /**
         * The default layer that is used to hold all features without a selected layer. It cannot be deleted but is
         * hidden when there are no features in the layer.
         */
        let defaultLayer;

        /**
         * An array containing the layers of the map. The order of the layers is defined by this array.
         * @type {[]}
         */
        let layers = [];

        /**
         * Dictionary that converts the name of a layer to the id of the layer in the layer array for fast access.
         * @type {{}}
         */
        let nameToLayer = {};

        /**
         * Dictionary containing a mapping of measurement ids to features.
         * @type {{}}
         */
        let featuresByMeasurementId = {};

        /**
         * Dictionary containing a mapping of node ids to features.
         * @type {{}}
         */
        let featuresByNodeId = {};

        /**
         * Is the map currently loading in data?
         * @type {boolean}
         */
        let loading = false;

        //Image overlays
        let overlays = [];
        let currentSelectedOverlay;
        let overlaysLocked = false;

        //State
        let drawingShape = false;
        let firstVertexDrawn = false;

        //Features
        let nextFeatureId = 1;
        let currentActiveFeature = null;

        // Tooltip
        // Visible when placing regions / markers on the map.
        let tooltip = $('<div class="managee-leaflet-edit-tooltip"></div>').insertAfter($(this));

        // Ajax Requests
        let lastLoadedFeature = null;

        //Edit Popup
        let popup = $('#contentPopup');
        let jsonPopup = $('#jsonPopup');

        //Media Popup
        let mediaSelectFrame;

        //Dictionary of id to feature
        let features = {}

        //list of feature ids that where edited in this session
        let editedFeatureContents = [];

        /**
         * Create a layer and all of its required data structures.
         */
        let createLayer = function(name, features = []) {
            let featureLayer = isEditor?L.featureGroup(features):L.markerClusterGroup(features);
            features.forEach(feature => {
                featureLayer.addLayer(feature);
            });
            return {
                name: name,
                _visible: true,
                features: featureLayer,
                setVisible : function(visible) {
                    this._visible = visible;
                    if(this._visible) {
                        map.addLayer(this.features);
                        this.features.eachLayer(layer => {
                            updateFeatureVisuals(layer);
                        });
                    } else {
                        map.removeLayer(this.features);
                    }
                },
                toggleVisible: function() {
                    this.setVisible(!this._visible);
                }
            };
        }

        /**
         * Add a layer to the layer data structure.
         * @param layer
         */
        let addLayer = function(layer) {
            nameToLayer[layer.name] = layers.length;
            layers.push(layer);
            layer.features.addTo(map);
            updateEditLayerSelect();
        }

        /**
         * Swap the position of two layers to reorder them in the list view.
         * @param layer1
         * @param layer2
         */
        let swapLayers = function(layer1, layer2) {
            if(layer1 === layer2) {
                return;
            }
            let index1 = layers.indexOf(layer1);
            let index2 = layers.indexOf(layer2);
            if(index1 === -1 || index2 === -1) return;
            layers[index1] = layers.splice(index2, 1, layers[index1])[0];
            updateEditLayerSelect();
        }

        /**
         * Initialize the default layer.
         */
        let initLayers = function() {
            defaultLayer = createLayer("Default");
            addLayer(defaultLayer);
        }

        // Selecting Features
        /**
         * Set a markers color and Icon depending on its state (selected/deselected)
         * @param markerLayer
         * @param color
         * @param borderColor
         * @param selected
         * @param iconUrl
         * @constructor
         */
        let UpdateMarker = function (markerLayer, color, borderColor, selected, iconUrl) {
            markerLayer.setIcon(selected ? selectedIcon : idleIcon);
            $(markerLayer._icon).css('background-color', color);
            $(markerLayer._icon).css('color', borderColor);
            $(markerLayer._icon).css("background-image", "url(" + iconUrl + ")");
        }

        /**
         * Event function called when a feature becomes active
         * @param layerEvent
         */
        let setFeatureActive = function (layerEvent) {
            setFeatureState(layerEvent.layer, true);
        }

        /**
         * Event function called when a feature becomes inactive
         * @param layerEvent
         */
        let setFeatureInactive = function (layerEvent) {
            setFeatureState(layerEvent.layer, false);
        }

        /**
         * Depending on the feature type and state, set its visual styles. This is called by
         * setFeatureActive and setFeatureInactive
         * @param layer
         * @param active
         */
        let setFeatureState = function (layer, active) {
            if (layer === null || layer === undefined || layer.feature === undefined) return;
            layer.feature.properties.active = active;
            updateFeatureVisuals(layer);
        }

        let updateFeatureVisuals = function (layer) {
            let active = layer.feature.properties.active === true;
            let geometry = layer.toGeoJSON().geometry;
            switch (geometry.type) {
                case "Point":
                    UpdateMarker(layer, layer.feature.properties.color, active ? selectedFeatureBorderColor : (increaseHexBrightness(layer.feature.properties.color, 20) + 'aa'), active, layer.feature.properties.iconUrl);
                    break;
                case "Polygon":
                    layer.setStyle({fillColor: layer.feature.properties.color, color: (active && !isEditor) ? selectedFeatureBorderColor : increaseHexBrightness(layer.feature.properties.color, 20)});
                    break;
            }
        }

        /**
         * Called when a layer should be selected. Sets the currentActiveLayer and updates the
         * UI using updateLayerInfo()
         * @param feature
         * @param center whether or not to focus on the feature
         */
        let selectFeature = function (feature, center = false) {
            map.invalidateSize();
            if (currentActiveFeature !== null) {
                setFeatureState(currentActiveFeature, false);
                currentActiveFeature.disableEdit();
            }
            currentActiveFeature = feature;
            if (currentActiveFeature !== null) {
                deselectOverlay();
                if(center === true) {
                    let layer = getLayerOfFeature(feature);
                    if (feature.getBounds !== undefined) {
                        //Polygon
                        let bounds = feature.getBounds();
                        map.fitBounds(bounds);
                        //map.panTo(bounds.getCenter());
                    } else {
                        //Marker
                        if(!isEditor) {
                            layer.features.zoomToShowLayer(feature);
                        }
                        map.panTo(feature.getLatLng());
                    }
                    layer.setVisible(true);
                    if(isEditor) {
                        feature.enableEdit();
                    }
                }
                closeMenu(true);
                showInfo(true);
            } else {
                closeInfo();
            }
            setFeatureState(feature, true);
            updateLayerInfo();
        }

        //Editor UI
        /**
         * Create a select for layers with the given selectedLayer as the current element.
         * @returns {string}
         */
        let getLayerSelect = function (selectedLayer = null) {
            let select = $('<select name="layer" id="layer"></select>');
            let i = 0;
            layers.forEach(layer => {
                select.append($('<option value="' + i + '" ' + (selectedLayer === layer ? 'selected' : '') + '>' + layer.name + '</option>'));
                i++;
            });
            return select;
        }

        /**
         * Create a select for measurement nodes
         * @returns {string}
         */
        let getMeasurementNodeSelect = function () {
            nodeSelect = $('<select></select>');
            nodeSelect.append($('<option value=""></option>'));
            // noinspection JSUnresolvedVariable
            for(let i = 0; i < measurementNodesList.length; i++) {
                let node = measurementNodesList[i];
                let id = node['id'];
                let prefix = '';
                for(let j = 0; j < node['depth']; j++) {
                    prefix += '&nbsp;';
                }
                let name = prefix + node['name'];
                nodeSelect.append($('<option value="' + id + '">' + name + '</option>'));
            }
            nodeSelect.on('change', function() {
                updateSelectedNodeMeasurementNode($(this).val());
            });
            return nodeSelect;
        }

        let updateSelectedNodeMeasurementNode = function(newId) {
            currentActiveFeature.feature.properties.measurementNodeId = newId;
            featuresByMeasurementId[newId] = currentActiveFeature;
            updateLayerInfo();
        }

        /**
         * Get the layer that a given feature is on
         * TODO speed this up using a dictionary of feature -> layer?
         * @param feature
         * @returns L.Layer|null the layer of the feature or null if it is not in any layer.
         */
        let getLayerOfFeature = function (feature) {
            let found = null;
            layers.forEach(layer => {
                if(layer.features.hasLayer(feature)) {
                    found = layer;
                }
            });
            return found;
        }

        /**
         *
         * @param layer
         * @returns {jQuery|HTMLElement}
         */
        let getEditNameInput = function (layer) {
            let input = $('<input type="text" name="name" id="name" value="' + layer.feature.properties.name + '"/>');
            input.on('change', function () {
                currentActiveFeature.feature.properties.name = input.val();
                updateLayerInfo();
            });
            return input;
        }

        /**
         * Show a popup with an editor for the content of a node.
         * @param featureId the id of the feature that we are editing
         */
        let showPopup = function (featureId) {
            popup.addClass('active');
            popup.find('.save-button').on('click', function () {
                let newContent = getEditorContent();
                // noinspection JSUnresolvedVariable
                $.post(endpoint_put_node_content.url, {
                    action: endpoint_put_node_content.action,
                    nonce: endpoint_put_node_content.nonce,
                    content: newContent,
                    mapId: mapId,
                    nodeId: featureId
                }, function() {
                    // add to list of edited objects.
                    if(editedFeatureContents.indexOf(featureId) === -1) {
                        editedFeatureContents.push(featureId);
                    }
                    lastLoadedFeature = null;
                    updateLayerInfo();
                    hidePopup();
                });
            });
            popup.find('.cancel-button').on('click', function () {
                hidePopup();
            });
        }

        /**
         * hide a popup
         */
        let hidePopup = function () {
            popup.removeClass('active');
            popup.find('.save-button').unbind('click');
            popup.find('.cancel-button').unbind('click');
        }

        /**
         * Show a popup with an editor for the content of a node.
         */
        let showJsonPopup = function () {
            jsonPopup.find('.geoJsonField').val('');
            jsonPopup.addClass('active');
            let layerSelectContainer = jsonPopup.find('.layer-select-container');
            layerSelectContainer.html("");
            let layerSelect = getLayerSelect();
            layerSelectContainer.append(layerSelect);
            jsonPopup.find('.save-button').on('click', function () {
                try {
                    let content = jsonPopup.find('.geoJsonField').val();
                    importGeoJson(JSON.parse(content), layers[layerSelect.val()], true);
                } catch(e) {
                    console.log(e);
                    alert(e);
                }
                hideJsonPopup();
            });
            jsonPopup.find('.cancel-button').on('click', function () {
                hideJsonPopup();
            });
        }

        /**
         * hide a popup
         */
        let hideJsonPopup = function () {
            jsonPopup.removeClass('active');
            jsonPopup.find('.save-button').unbind('click');
            jsonPopup.find('.cancel-button').unbind('click');
        }

        let setEditorContent = function (content, editor_id, textarea_id) {
            if (typeof editor_id == 'undefined') editor_id = wpActiveEditor;
            if (typeof textarea_id == 'undefined') textarea_id = editor_id;

            if ($('#wp-' + editor_id + '-wrap').hasClass('tmce-active') && tinyMCE.get(editor_id)) {
                return tinyMCE.get(editor_id).setContent(content);
            } else {
                return $('#' + textarea_id).val(content);
            }
        };

        let getEditorContent = function () {
            let content;
            let inputId = 'mangee_map_marker_editor';
            let editor = tinyMCE.get(inputId);
            let textArea = $('textarea#' + inputId);
            if (textArea.length > 0 && textArea.is(':visible')) {
                content = textArea.val();
            } else {
                content = editor.getContent();
            }
            return content;
        };

        /**
         * Update the info views for the currentActiveLayer. If the currentActiveLayer if null, show the
         * "Select a node" message
         */
        let updateLayerInfo = function () {
            if (currentActiveFeature !== null) {
                let name = currentActiveFeature.feature.properties.name;
                let displayedName = name;
                let measurementNodeId = currentActiveFeature.feature.properties.measurementNodeId;
                /*if(measurementNodeId !== undefined && measurementNodeId != null && measurementNodeId !== "") {
                    displayedName += "<br><small>" + measurementNodesById[measurementNodeId]['name'] + "</small>";
                }*/
                infoElementTitle.html(displayedName);
                detailSpan.html(name);
                detailSpan.removeClass("hidden");
                detailButton.removeClass("hidden");
                if(isEditor) {
                    editTitle.html("Edit Node " + name);
                    let copyButton = $('<a href="#" style="margin-left: 10px;"><small>copy link</small></a>');
                    copyButton.on("click", function() {
                        let text = '<a href="#" onclick="parent.showNodeById(' + currentActiveFeature.feature.properties.id + ');">' + name + '</a>';
                        navigator.clipboard.writeText(text).then(function() {
                            console.log('Async: Copying to clipboard was successful!');
                        }, function(err) {
                            console.error('Async: Could not copy text: ', err);
                        });
                    });
                    editTitle.append(copyButton);
                    editName.val(name);
                    editLayer.val(layers.indexOf(getLayerOfFeature(currentActiveFeature)));
                    measurementNodeSelect.val(measurementNodeId);
                    editColor.spectrum("set", currentActiveFeature.feature.properties.color);
                    if(currentActiveFeature.feature.geometry !== undefined && currentActiveFeature.feature.geometry.type === "Point") {
                        editIcon.val(currentActiveFeature.feature.properties.iconUrl);
                        editIconContainer.show();
                        editIconMessage.hide();
                    } else {
                        editIconContainer.hide();
                        editIconMessage.show();
                    }
                    editTable.show();
                }
                loadContentUrl(currentActiveFeature);
            } else {
                cancelCurrentRequest();
                if(isEditor) {
                    editTitle.html("Select a feature to edit its Properties.");
                    editTable.hide();
                }
                infoElementTitle.html("Select node to view its Data");
                lastLoadedFeature = null;
                detailSpan.addClass("hidden");
                detailButton.addClass("hidden");
            }

            if(isEditor) {
                jsonContent.html(JSON.stringify(getGeoJson(false)));
            }
            //TODO call this only if necessary!
            updateLayers();
        }

        let updateLayers = function() {
            layerElement.html("");
            let layerTitle = $('<span class="title">Layers</span>');

            if(isEditor) {
                let addButton = $('<span class="dashicons dashicons-plus" style="float: right; cursor: pointer;"></span>');
                layerTitle.append(addButton);
                addButton.on('click', function() {
                    let name = window.prompt("Enter a name for the new Layer","");
                    let ok = true;
                    if(name !== "" && name !== null && name !== undefined) {
                        layers.forEach(layer => {
                            if(layer.name === name) {
                                ok = false;
                            }
                        });
                        if(ok) {
                            addLayerGroup(name, []);
                        }
                    }
                });
            }

            layerElement.append(layerTitle);
            for (let layerIdString in layers) {
                if (!layers.hasOwnProperty(layerIdString)) continue;
                let layerId = parseInt(layerIdString);
                let layer = layers[layerId];
                if(layerId === 0 && layer.features.getLayers().length === 0) continue;
                let parentContainer = $('<div class="layer-container"></div>');
                let layerContainer = $('<div class="layer-child-container"></div>');
                let visible = 'dashicons-visibility';
                let invisible = 'dashicons-hidden';
                let iconClass = layer._visible ? visible : invisible;
                let title = $('<div class="layer-title">' + layer.name + '</div>');
                if(isEditor && layerId !== 0) {
                    let removeIcon = $('<span class="dashicons dashicons-trash" style="width: 25px;"></span>');
                    removeIcon.on('click', function () {
                        if(confirm("Do you want to delete Layer \""+layer.name + "\"")) {
                            let tempFeatures = [];

                            layer.features.eachLayer(function(feature) {
                                tempFeatures.push(feature);
                            });

                            tempFeatures.forEach(feature => {
                                layer.features.removeLayer(feature);
                                // 2. add to new layer
                                defaultLayer.features.addLayer(feature);
                                updateFeatureVisuals(feature);
                            });

                            //TODO move this to a function that sorts all the layers correctly!
                            layers.splice(layerId, 1);
                            updateEditLayerSelect();

                            updateLayerInfo();
                            updateLayers();
                        }
                    });
                    //Reorder layers that are not default and not overlays.
                    if(layers.length > layerId + 1) {
                        let downIcon = $('<span class="dashicons dashicons-arrow-down" style="width: 25px;"></span>');
                        downIcon.on('click', function() {
                            swapLayers(layer, layers[layerId + 1]);
                            updateLayers();
                        });
                        title.prepend(downIcon);
                    }
                    if(layerId > 1) {
                        let upIcon = $('<span class="dashicons dashicons-arrow-up" style="width: 25px;"></span>');
                        upIcon.on('click', function() {
                            swapLayers(layer, layers[layerId - 1]);
                            updateLayers();
                        });
                        title.prepend(upIcon);
                    }
                    title.prepend(removeIcon);
                }

                let visibilityIcon = $('<span class="dashicons ' + iconClass + '" style="width: 25px;"></span>');
                title.prepend(visibilityIcon);
                visibilityIcon.on('click', function () {
                    selectFeature(null);
                    layer.toggleVisible();
                    updateLayers();
                });
                parentContainer.append(title);
                parentContainer.append(layerContainer);
                if(layer._visible) {
                    let layersArray = [];
                    layer.features.eachLayer(function(feature) {
                        layersArray.push(feature);
                    });

                    layersArray = layersArray.sort((a, b) => {
                        let nameA = a.feature.properties.name;
                        let nameB = b.feature.properties.name;
                        let numberA = parseFloat(nameA.replace(/\D/g,''));
                        let numberB = parseFloat(nameB.replace(/\D/g,''));
                        let firstTextA = nameA.match(/[a-zA-Z]+/g)[0];
                        let firstTextB = nameB.match(/[a-zA-Z]+/g)[0];
                        return (firstTextA === firstTextB) ? ((numberA > numberB) ? 1 : -1) : nameA.localeCompare(nameB);
                    });

                    for(let i = 0; i < layersArray.length; i++) {
                        let feature = layersArray[i];
                        let activeClass = currentActiveFeature === feature?" active":"";
                        let featureElement = $('<div class="layer'+activeClass+'">' + feature.toGeoJSON().properties.name + '</div>');
                        featureElement.on('click', function () {
                            if(isEditor) {
                                if (!feature.editEnabled) {
                                    feature.enableEdit();
                                }
                            }
                            selectFeature(feature, true);
                        });
                        layerContainer.append(featureElement);
                    }
                }

                layerElement.append(parentContainer);
            }
            if(isEditor && overlays.length > 0) {
                let layerContainer = $('<div class="layer-container"></div>');
                let locked = 'dashicons-lock';
                let unlocked = 'dashicons-unlock';
                let iconClass = overlaysLocked ? locked : unlocked;
                let title = $('<div class="layer-title"><span class="dashicons ' + iconClass + '" style="width: 25px;"></span>Overlays</div>');
                title.on('click', function () {
                    toggleOverlaysActive();
                    updateLayers();
                });
                layerContainer.append(title);
                for (let overlayId in overlays) {
                    if (!overlays.hasOwnProperty(overlayId)) continue;
                    let overlay = overlays[overlayId];
                    let featureElement = $('<div class="layer">' + overlay.name + '</div>');
                    featureElement.on('click', function () {
                        selectOverlay(overlay.image);
                    });
                    layerContainer.append(featureElement);
                }
                layerElement.append(layerContainer);
            }
        }

        //Tooltip Events
        /**
         * Called when the tooltip is initialized for the initial click on the map
         * @param e
         */
        let addTooltip = function (e) {
            drawingShape = true;
            L.DomEvent.on(document, 'mousemove', moveTooltip);
            if (e.layer._bounds === undefined) {
                //Marker
                tooltip.html('Click on the map to place the Marker');
            } else {
                tooltip.html('Click on the map to start marking a region');
            }
            tooltip.css("display", 'block');
        }

        /**
         * called when the tooltip will be removed from the map
         */
        let removeTooltip = function () {
            drawingShape = false;
            firstVertexDrawn = false;
            tooltip.html('');
            tooltip.css("display", 'none');
            L.DomEvent.off(document, 'mousemove', moveTooltip);
            //updateLayerInfo();
        }

        /**
         * Called when the cursor is moved along the map while the tooltip is active. Set the absolute positions
         * to follow the cursor.
         * @param e
         */
        let moveTooltip = function (e) {
            let mapPosition = mapElement.offset();
            tooltip.css('left', e.clientX - mapPosition.left + 'px');
            tooltip.css('top', e.clientY - mapPosition.top + 'px');
        }

        /**
         * Called when we clicked on the map. Update the message according to the selected tool
         * @param e
         */
        let updateTooltip = function (e) {
            if (e.layer.editor._drawnLatLngs === undefined) {
                // A Marker was placed
                return;
            }
            if (e.layer.editor._drawnLatLngs.length < e.layer.editor.MIN_VERTEX - 1) {
                tooltip.html('Click on the map to continue drawing region.');
                firstVertexDrawn = true;
            } else {
                tooltip.html('Click on start point or "Enter" to finish Region');
            }
            updateLayerInfo();
        }

        /**
         * Called when the user cancels drawing a marker/polygon before completing it.
         */
        let cancelDrawing = function (layerEvent) {
            removeFeature(layerEvent.layer);
        }

        let commitDrawing = function (layerEvent) {
            let layer = layerEvent.layer;
            if (layer.editor._drawnLatLngs === undefined) {
                addFeature(layerEvent.layer, defaultLayer);
                return;
            }
            if (layer.editor._drawnLatLngs.length <= layer.editor.MIN_VERTEX - 1) {
                console.log("Removing because the feature does not have enough vertices");
                removeFeature(layer);
            } else {
                addFeature(layerEvent.layer, defaultLayer);
            }
        }

        /**
         * Add a new Group to the Map
         * @param layerName
         * @param features
         */
        let addLayerGroup = function (layerName, features = []) {
            let newLayer = createLayer(layerName, features);
            addLayer(newLayer);
            updateLayers();
        }

        /**
         * Add a new Feature to the map
         * @param featureLayer
         * @param layer
         * @param newIds {boolean} override the features id with a new id. Used when importing external GeoJSON.
         */
        let addFeature = function (featureLayer, layer, newIds, select = true) {
            if (featureLayer.feature === undefined) {
                featureLayer.feature = {};
            }

            if (featureLayer.feature.type === undefined) {
                featureLayer.feature.type = "Feature";
            }

            if (featureLayer.feature.properties === undefined) {
                featureLayer.feature.properties = {};
            }

            if (featureLayer.feature.properties.id === undefined || newIds) {
                featureLayer.feature.properties.id = nextFeatureId;
                nextFeatureId++;
            } else {
                if (parseInt(featureLayer.feature.properties.id) >= nextFeatureId) {
                    nextFeatureId = parseInt(featureLayer.feature.properties.id) + 1;
                }
            }
            if(features[featureLayer.feature.properties.id] !== undefined) {
                console.log("Error: duplicate feature id " + featureLayer.feature.properties.id);
            } else {
                //console.log("Added Feature with id: " + featureLayer.feature.properties.id);
            }
            features[featureLayer.feature.properties.id] = featureLayer.feature;
            featuresByNodeId[featureLayer.feature.properties.id] = featureLayer;

            if (featureLayer.feature.properties.name === undefined) {
                featureLayer.feature.properties.name = "Feature " + featureLayer.feature.properties.id;
            }

            if (featureLayer.feature.properties.color === undefined) {
                featureLayer.feature.properties.color = '#0090ff';
            }

            if (featureLayer.feature.properties.layer !== undefined) {
                let found = false;
                layers.forEach(layer => {
                    if (layer.name === featureLayer.feature.properties.layer) {
                        featureLayer.addTo(layer.features);
                        found = true;
                    }
                });
                if (!found) {
                    addLayerGroup(featureLayer.feature.properties.layer, [featureLayer]);
                }
            } else {
                featureLayer.addTo(layer.features);
            }
            if(featureLayer.feature.properties.measurementNodeId !== undefined) {
                featuresByMeasurementId[featureLayer.feature.properties.measurementNodeId] = featureLayer;
            }

            featureLayer.on('click', function () {
                if (currentActiveFeature !== null) {
                    currentActiveFeature.disableEdit();
                }
                selectFeature(featureLayer);
                if(isEditor) {
                    featureLayer.enableEdit()
                }
            });
            //Update info when the layer was dragged to a different position using Path.Drag
            featureLayer.on('dragend', function () {
                updateLayerInfo();
            });
            if(select) {
                selectFeature(featureLayer);
            }
        }

        /**
         * Remove a feature from the map and all corresponding layers.
         */
        let removeFeature = function (feature) {
            if (feature === null || feature === undefined) return;
            let layer = getLayerOfFeature(feature);
            if (layer !== null) {
                layer.features.removeLayer(feature);
            }
            feature.remove();
            updateLayers();
        }

        /**
         * Export the maps geo json data.
         * @param addLayerData {boolean} should additional non-geo JSON be added to support layer saving?
         */
        let getGeoJson = function (addLayerData) {
            let geoJson = {
                type: 'FeatureCollection',
                features: []
            };
            let layerData = [];
            layers.forEach(layer => {
                layerData.push({
                    name: layer.name,
                    visible: layer._visible
                });
                layer.features.eachLayer(function(feature) {
                    let featureJson = feature.toGeoJSON();
                    delete (featureJson.properties.visible);
                    featureJson.properties.layer = layer.name;
                    geoJson.features.push(featureJson);
                });
            });
            overlays.forEach(overlay => {
                let corners = overlay.image.getCorners();
                //let angle = overlay.image.getAngle();
                let coordinates = [
                    [
                        [corners[0].lng, corners[0].lat],
                        [corners[2].lng, corners[2].lat],
                        [corners[3].lng, corners[3].lat],
                        [corners[1].lng, corners[1].lat],
                        [corners[0].lng, corners[0].lat],
                    ]
                ]
                geoJson.features.push({
                    "type": "Feature",
                    "geometry": {
                        "type":"Polygon",
                        "coordinates": coordinates
                    },
                    "properties": {
                        name: overlay.name,
                        isOverlay: true,
                        url: overlay.image._url,
                        //"angle":angle
                    }
                });
            });
            if(addLayerData) {
                geoJson.layerData = layerData;
                geoJson.extraData = {
                    nextFeatureId: nextFeatureId
                };
            }
            return geoJson;
        }

        let publishMap = function() {
            let data = {
                'action': endpoint_put_geo_json.action,
                'nonce': endpoint_put_geo_json.nonce,
                'map_id': mapId,
                'map_name': mapName,
                'map_tile_url': tileLayer._url,
                'geo_json': getGeoJson(true),
                'edited_nodes':editedFeatureContents
            };
            // We can also pass the url value separately from ajaxurl for front end AJAX implementations
            // noinspection JSUnresolvedVariable
            $.post(endpoint_put_geo_json.url, data, function () {
                saveResponse.show();
                setTimeout(function() {
                    saveResponse.hide();
                }, 3000);
            });
        }

        let updateEditLayerSelect = function(){
            if(!isEditor) return;
            let parent = editLayer.parent();
            editLayer.remove();
            editLayer = getLayerSelect();
            editLayer.on('change', function () {
                if(editTable.is(":visible")) { // Only do this once the view is shown to prevent setting the layer on selection change
                    // Move to different layer
                    // 1. remove from old layer
                    getLayerOfFeature(currentActiveFeature).features.removeLayer(currentActiveFeature);
                    // 2. add to new layer
                    let newLayerId = editLayer.val();
                    layers[newLayerId].features.addLayer(currentActiveFeature);
                    updateLayerInfo();
                }
            });
            parent.append(editLayer);
        }

        let showMenu = function() {
            rootContainer.addClass("menu-visible");
        }

        let closeMenu = function(mobileOnly = false) {
            if(!mobileOnly || rootContainer.hasClass("small")) {
                rootContainer.removeClass("menu-visible");
                closeInfo();
            }
        }

        let searchMenu = function() {
            rootContainer.addClass("menu-visible");
        }

        let showInfo = function(desktopOnly) {
            if(!desktopOnly || !rootContainer.hasClass("small")) {
                showMenu();
                rootContainer.addClass("info-visible");
            }
        }

        let closeInfo = function() {
            rootContainer.removeClass("info-visible");
        }

        /**
         * Called when the content of the info element was loaded
         */
        let onLoadedContent = function () {
            infoElementContent.show();
            infoElementLoader.hide();
        }

        /**
         * Used to load data into the right content column.
         * @param layer
         */
        let loadContentUrl = function(layer) {
            if(loading) return;
            if (lastLoadedFeature !== currentActiveFeature) {
                cancelCurrentRequest();
                if(layer === undefined || layer === null) {
                    return;
                }
                infoElementLoader.show();
                let showEdited = editedFeatureContents.indexOf(layer.feature.properties.id) !== -1;
                let measurementNode = currentActiveFeature.feature.properties.measurementNodeId;
                // noinspection JSUnresolvedVariable
                let url = backend.root_url + '/managee-maps/node-content/?map_id=' + mapId + '&node_id=' + currentActiveFeature.feature.properties.id + '&latest=' + showEdited + "&measurement_node=" + measurementNode;
                infoElementContent.attr('src',url);
                lastLoadedFeature = currentActiveFeature;
            }
        }
        /**
         * Cancel loading the current content
         */
        let cancelCurrentRequest = function () {
            infoElementContent.attr('src',"");
            infoElementContent.hide();
            infoElementLoader.hide();
        }

        /**
         * Create and link the required html for the plugin
         */
        let createDom = function () {
            mainContainer = $('<div class="main-container"></div>');

            mapElement = $('<div class="map-column" id="managee-map-' + currentMapId + '" style="height: ' + mapHeight + ';"></div>');

            mapMenu = $('<div class="map-menu"></div>');

            let closeButton = $('<div class="menu-button close-button"><span class="dashicons dashicons-no"></span></div>');
            closeButton.on('mousedown', function() {closeMenu(false)});
            mapMenu.append(closeButton);

            let menuButton = $('<div class="menu-button open-button"><span class="dashicons dashicons-menu-alt3"></span></div>');
            menuButton.on('mousedown', showMenu);
            mapMenu.append(menuButton);

            /*let searchButton = $('<div class="menu-button search-button"><span class="dashicons dashicons-search"></span></div>');
            searchButton.on('mousedown', searchMenu);
            mapMenu.append(searchButton);*/

            detailSpan = $('<div class="detail-span"></div>');
            detailSpan.on('mousedown', function() {showInfo(false);});
            mapMenu.append(detailSpan);

            detailButton= $('<div class="menu-button detail-button"><span class="dashicons dashicons-info"></span></div>');
            detailButton.on('mousedown', function() {showInfo(false);});
            mapMenu.append(detailButton);


            mapElement.append(mapMenu);

            currentMapId++;
            mainContainer.append(mapElement);

            layerElement = $('<div class="layer-column" style="height: ' + mapHeight + ';"><span class="title">Layer</span></div>');
            mainContainer.append(layerElement);

            infoElement = $('<div class="info-column" style="height: ' + mapHeight + ';"></div>');

            infoElementTitle = $('<span class="title">Select a node to view it\'s contents</span>');
            infoElement.append(infoElementTitle);
            infoElementContent = $('<iframe class="content"></iframe>');
            infoElementContent.on('load',function() {
                onLoadedContent();
            });

            infoElement.append(infoElementContent);

            let closeInfoButton = $('<div class="close-info-button"><span class="dashicons dashicons-no"></span> Zurück zur Karte</div>');
            closeInfoButton.on('mousedown', function() {
                closeMenu();
            });
            infoElement.append(closeInfoButton);

            infoElementLoader = $('<div class="loader-container"><div class="loader"></div><br>Wird geladen...</div>');
            infoElement.append(infoElementLoader);

            mainContainer.append(infoElement);

            rootContainer.append(mainContainer);
            if(isEditor) {
                let editContainer = $('<div class="edit-container"></div>');
                saveResponse = $('<span class="save-response"><span class="dashicons dashicons-saved" style="margin-top: 3px;"></span> Map was published successfully</span>');
                rootContainer.prepend(saveResponse);
                saveResponse.hide();
                let publishButton = $('<button class="button publish-button" style="margin-bottom: 10px;"><span class="dashicons dashicons-cloud-upload" style="margin-top: 3px;"></span> Publish Map</button>');
                publishButton.on('click', publishMap);
                rootContainer.prepend(publishButton);
                editView = $('<div class="edit-column"></div>');

                editTitle = $('<span class="title"></span>');
                editView.append(editTitle);

                editTable = $('<table class="wp-list-table widefat fixed striped">' +
                    '<tr><td style="width: 100px;"><label for="name-input">Name</label></td><td class="name-input-container"><input type="text" class="large-text" id="name-input"></td></tr>' +
                    '<tr><td style="width: 100px;"><label for="layer">Layer</label></td><td class="layer-select-container"></td></tr>' +
                    '<tr><td style="width: 100px;"><label for="layer">Color</label></td><td class="color-container"></td></tr>' +
                    '<tr><td style="width: 100px;"><label for="node-select">Messstelle</label></td><td class="node-select-container"></td></tr>' +
                    '<tr><td style="width: 100px;"><label for="layer">Icon URL</label></td><td class="icon-input-message"><input type="text" class="large-text" value="Only markers can have icons." disabled></td><td class="icon-input-container"><input class="large-text"  type="text" id="icon-input"></td></tr>' +
                    '<tr><td style="width: 100px;"><label for="editInfoButton">Info Content</label></td><td><button class="button" id="editInfoButton">Edit</button></td></tr>' +
                    '</table>');

                editName = editTable.find('#name-input')
                editName.on('change', function() {
                    if(editTable.is(":visible")) { // Only do this once the view is shown to prevent setting the layer on selection change
                        currentActiveFeature.feature.properties.name = $(this).val();
                        updateLayerInfo();
                    }
                });
                editLayer = getLayerSelect();
                editTable.find('.layer-select-container').append(editLayer);
                editInfoContentButton = editTable.find('#editInfoButton');

                measurementNodeSelect = getMeasurementNodeSelect();
                editTable.find('.node-select-container').append(measurementNodeSelect);

                editColor = $('<input type="text" id="nodeColorSelect"/>');
                editTable.find('.color-container').append(editColor);

                editColor.spectrum({
                    color: '#ffffff',
                    preferredFormat: "hex",
                    showInput: true,
                    showButtons: false,
                    move: function(color) {
                        currentActiveFeature.feature.properties.color = "#" + color.toHex();
                        updateFeatureVisuals(currentActiveFeature);
                    },
                    hide: function() {
                        updateLayerInfo();
                    }
                });


                editIconContainer = editTable.find('.icon-input-container');
                editIconMessage = editTable.find('.icon-input-message');
                editIcon = editIconContainer.find('#icon-input');

                editIcon.on('change', function() {
                    if(editTable.is(":visible")) { // Only do this once the view is shown to prevent setting the layer on selection change
                        currentActiveFeature.feature.properties.iconUrl = $(this).val();
                        updateLayerInfo();
                        updateFeatureVisuals(currentActiveFeature);
                    }
                });

                editInfoContentButton.on('click', function() {
                    console.log("Show edit view");
                    let data = {
                        'action': endpoint_get_node_content.action,
                        'nonce': endpoint_get_node_content.nonce,
                        'map_id': mapId,
                        'node_id': currentActiveFeature.feature.properties.id,
                        'edited': editedFeatureContents.indexOf(currentActiveFeature.feature.properties.id) !== -1
                    };
                    // We can also pass the url value separately from ajaxurl for front end AJAX implementations
                    // noinspection JSUnresolvedVariable
                    $.post(endpoint_get_node_content.url, data, function (response) {
                        setEditorContent(response, 'mangee_map_marker_editor', 'mangee_map_marker_editor');
                        showPopup(currentActiveFeature.feature.properties.id);
                    });
                });
                editView.append(editTable);
                editContainer.append(editView);
                jsonView = $('<div class="json-column"></div>');
                jsonContent = $('<textarea readonly rows="3" class="content"></textarea>');

                let addJsonButton = $('<button class="button" style="margin-bottom: 5px;"><span class="dashicons dashicons-plus" style="margin-top: 5px;"></span> Import GeoJSON</button>');
                addJsonButton.on('click', function() {
                    showJsonPopup();
                });
                jsonView.append(addJsonButton);
                jsonView.append(jsonContent);

                settingsView = $('<div class="settings-column">' +
                    '    <table class="wp-list-table widefat fixed striped">\n' +
                    '        <tr>\n' +
                    '            <td class="id-column" style="width: 100px;">\n' +
                    '                <label for="map-name">Map Name</label>\n' +
                    '            </td>\n' +
                    '            <td>\n' +
                    '                <input type="text" class="large-text" name="map-name" id="map-name" value="' + mapName + '"/>\n' +
                    '            </td>\n' +
                    '        </tr>\n' +
                    '        <tr>\n' +
                    '            <td class="id-column">\n' +
                    '                <label for="tile-url">Tile Server URL</label>\n' +
                    '            </td>\n' +
                    '            <td>\n' +
                    '                <input type="text" class="large-text" name="tile-url" id="tile-url" value=""/>\n' +
                    '            </td>\n' +
                    '        </tr>\n' +
                    '    </table>' +
                    '</div>');

                settingsView.find('#tile-url').on('change', function() {
                    tileLayer.setUrl($(this).val());
                    checkTileLayerVisible();
                });

                settingsView.find('#map-name').on('change', function() {
                    mapName = $(this).val();
                });

                editContainer.append(jsonView);

                rootContainer.append(editContainer);

                rootContainer.append(settingsView);


                let publishButton2 = $('<button class="button publish-button" style="margin-top: 20px; margin-bottom: 20px;"><span class="dashicons dashicons-cloud-upload" style="margin-top: 3px;"></span> Publish Map</button>');
                publishButton2.on('click', publishMap);
                rootContainer.append(publishButton2);
            }
        }

        let checkTileLayerVisible = function() {
            if(tileLayer._url === '') {
                map.removeLayer(tileLayer);
            } else {
                map.addLayer(tileLayer);
            }
        }

        let updateTileUrl = function() {
            settingsView.find('#tile-url').val(tileLayer._url);
        }

        let importGeoJson = function (GeoJson, layerToAddTo, newIds = false) {
            loading = true;
            let layerData = GeoJson.layerData;
            let extraData = GeoJson.extraData;
            delete(GeoJson.extraData);
            /**
             * Used to calculate the bounds of the newly added nodes to focus the view on all nodes once loaded.
             */
            const boundingFeatureGroup = new L.FeatureGroup();

            let children = {
                type: 'FeatureCollection',
                features: []
            };

            L.geoJson(GeoJson, {
                onEachFeature: function (feature, layer) {
                    if(feature.type === "GeometryCollection") {
                        let name = "Layer";
                        if(feature.properties !== undefined && feature.properties.name !== undefined) {
                            name = feature.properties.name;
                        }
                        feature.geometries.forEach(geometry => {
                            let featureJson = {
                                "type": "Feature",
                                "properties": {
                                    'name': name,
                                    'layer': layerToAddTo.name
                                },
                                "geometry": geometry
                            };
                            children.features.push(featureJson);
                        });
                        return;
                    }
                    if(feature.properties !== undefined && feature.properties.isOverlay) {
                        let coordinates = feature.geometry.coordinates[0];
                        let name = feature.properties.name;
                        let url = feature.properties.url;
                        layer.remove();
                        addOverlay([
                            L.latLng(coordinates[0][1],coordinates[0][0]),
                            L.latLng(coordinates[3][1],coordinates[3][0]),
                            L.latLng(coordinates[1][1],coordinates[1][0]),
                            L.latLng(coordinates[2][1],coordinates[2][0])
                        ], name, url, false);
                    } else {
                        try {
                            //Normal Feature import
                            if(!isEditor) {
                                layer.on('move', function() {
                                    updateFeatureVisuals(layer);
                                });
                                layer.on('add', function() {
                                    updateFeatureVisuals(layer);
                                });
                            }
                            layer.addTo(boundingFeatureGroup);
                            addFeature(layer, layerToAddTo, newIds, false);
                        } catch (e) {
                            console.log(e);
                        }
                    }
                }
            });

            //Deselect all layers at start.
            loading = false;
            selectFeature(null);
            deselectOverlay();
            overlaysLocked = true;
            updateOverlayEditors();
            updateLayers();
            if(children.features.length > 0) {
                importGeoJson(children, layerToAddTo, newIds);
            }
            try {
                map.fitBounds(boundingFeatureGroup.getBounds());
            } catch(e) {
                console.log("No Items imported!");
            }
            boundingFeatureGroup.remove();
            if(layerData !== undefined) {
                layerData.forEach(importedLayer => {
                    if(importedLayer.name === "Default") {
                        defaultLayer.setVisible(importedLayer.visible === "true" ||importedLayer.visible === true)
                    } else {
                        let found = false;
                        layers.forEach(layer => {
                            if (layer.name === importedLayer.name) {
                                found = true;
                                layer.setVisible(importedLayer.visible === "true" ||importedLayer.visible === true)
                            }
                        });

                        if(!found) {
                            let newLayer = createLayer(importedLayer.name, []);
                            addLayer(newLayer);
                            newLayer.setVisible(importedLayer.visible === "true" ||importedLayer.visible === true)
                        }
                    }
                });
                updateLayers();
            }
            if(extraData !== undefined) {
                nextFeatureId = extraData.nextFeatureId;
            }
        }

        let selectImage = function() {
            // If the media frame already exists, reopen it.
            if ( mediaSelectFrame ) {
                mediaSelectFrame.open();
                return;
            }

            // Create a new media frame
            mediaSelectFrame = wp.media({
                title: 'Select overlay Media',
                button: {
                    text: 'Create Overlay'
                },
                multiple: false  // Set to true to allow multiple files to be selected
            });


            // When an image is selected in the media frame...
            // noinspection JSCheckFunctionSignatures
            mediaSelectFrame.on( 'select', function() {
                // Get media attachment details from the frame state
                let attachment = mediaSelectFrame.state().get('selection').first().toJSON();
                // noinspection HtmlRequiredAltAttribute,RequiredAttributes
                $("<img/>",{
                    load : function(){
                        let center = map.getCenter();
                        let circle = L.circle(center, 50).addTo(map);
                        let bounds = circle.getBounds();
                        circle.remove();
                        let rel = (bounds.getNorth() - bounds.getSouth()) / (bounds.getEast() - bounds.getWest());
                        let widthOffset = this.width/2;
                        let heightOffset = this.height/2;

                        let scale = (map.getBounds().getNorth() - map.getBounds().getSouth()) / this.height;

                        widthOffset *= scale;
                        heightOffset *= (scale * rel);

                        let corners = [
                            L.latLng(center.lat + heightOffset, center.lng - widthOffset),
                            L.latLng(center.lat + heightOffset, center.lng + widthOffset),
                            L.latLng(center.lat - heightOffset, center.lng - widthOffset),
                            L.latLng(center.lat - heightOffset, center.lng + widthOffset)
                        ];

                        addOverlay(corners, attachment.name, attachment.url);
                    },
                    src  : attachment.url
                });
            });

            // Finally, open the modal on click
            mediaSelectFrame.open();
        };

        /**
         * Add an image overlay to the map.
         * @param corners
         * @param name
         * @param url
         * @param select
         */
        let addOverlay = function(corners, name, url, select = true) {
            deselectOverlay();
            let img = L.distortableImageOverlay(url,{
                corners: corners,
                selected: isEditor?select:false,
                editable: isEditor,
                suppressToolbar: true,
                mode: isEditor?'freeRotate':'lock',
                actions: isEditor?[L.FreeRotateAction]:[],
            }).addTo(map);

            $(img._image).unbind('mousedown').unbind('mouseup').unbind('mousemove');

            if(!isEditor) {
                console.log(img);
            }

            //img.setAngle(angle);
            img.on('edit',function() {
                updateLayerInfo();
            });

            let dragging = false;
            $(img._image).on("mousedown", function() {
                dragging = true;
                selectOverlay(img)
            });
            $(img._image).on("mouseup", function() {
                dragging = false;
                updateLayerInfo();
            });
            map.on("click",function() {
                if(!dragging) {
                    deselectOverlay();
                }
            });
            overlays.push({
                name: name,
                image: img
            });
            updateLayers();
            if(select) {
                setTimeout(() => {
                    overlaysLocked = false;
                    updateOverlayEditors();
                    updateLayers();
                    updateLayerInfo();
                }, 100)
            }
        }

        let selectOverlay = function(img) {
            selectFeature(null);
            img.select();
            currentSelectedOverlay = img;
            updateLayerInfo();
        }

        let deselectOverlay = function() {
            if(currentSelectedOverlay !== null && currentSelectedOverlay !== undefined && currentSelectedOverlay.isSelected()) {
                currentSelectedOverlay.deselect();
                currentSelectedOverlay = null;
            }
            updateLayerInfo();
        }

        let toggleOverlaysActive = function() {
            overlaysLocked = !overlaysLocked;
            updateOverlayEditors();
            updateLayers();
        }

        let updateOverlayEditors = function() {
            setTimeout(() => {
                overlays.forEach(overlay => {
                    if(overlay.image.editing._enabled === undefined || overlay.image.editing._enabled === overlaysLocked) {
                        if(overlaysLocked) {
                            overlay.image.editing.disable();
                        } else {
                            overlay.image.editing.enable();
                        }
                        // console.log(overlay.image.editing._enabled + ': ' +overlaysLocked);
                    }
                });
            }, 100);
        }

        // noinspection JSUnresolvedVariable
        /**
         * Main Entry point for the Plugin. Initialize the Map and all events.
         */
        let initialize = function () {

            // Create and link the required html for the plugin
            createDom();

            $(this).bind('showMeasurementNode', function(event, measurementNodeId) {
                let node = featuresByMeasurementId[measurementNodeId];
                if(node !== undefined) {
                    selectFeature(node, true);
                } else {
                    console.log("Could not find node with measurement id " + measurementNodeId);
                }
            });

            $(this).bind('showNodeById', function(event, nodeId) {
                let node = featuresByNodeId[nodeId];
                if(node !== undefined) {
                    selectFeature(node, true);
                } else {
                    console.log("Could not find node with measurement id " + nodeId);
                }
            });

            if(fullscreen) {
                rootContainer.addClass("fullscreen");
            }

            let updateSizeClass = function() {
                rootContainer.toggleClass("small", rootContainer.width() < 1024);
            }

            let updateFullscreenContainerSize = function() {
                let vh = window.innerHeight * 0.01;
                rootContainer[0].style.setProperty('--vh', `${vh}px`);
            }

            let updateSize = function() {
                updateSizeClass();
                if(fullscreen) {
                    updateFullscreenContainerSize();
                }
            }

            window.addEventListener('resize', () => {
                updateSize();
            });
            updateSize();

            tileLayer = L.tileLayer(tileUrl, {
                maxZoom: 20,
                maxNativeZoom: 18,
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Points &copy 2012 LINZ'
            });


            let latlng = L.latLng(-37.82, 175.24);


            map = L.map($(mapElement).attr('id'), {center: latlng, zoom: 13, layers: [tileLayer], editable: isEditor, maxZoom: 20});
            checkTileLayerVisible();

            initLayers();

            if(isEditor) {
                updateTileUrl();
            }

            if(isEditor) {
                // Custom Map controls
                L.EditControl = L.Control.extend({

                    options: {
                        position: 'topleft',
                        callback: null,
                        kind: '',
                        html: ''
                    },

                    onAdd: function (map) {
                        let container = L.DomUtil.create('div', 'leaflet-control leaflet-bar');
                        let link = L.DomUtil.create('a', '', container);
                        link.href = '#';
                        link.title = 'Create a new ' + this.options.kind;
                        link.innerHTML = this.options.html;
                        L.DomEvent.on(link, 'click', function (e) {
                            L.DomEvent.stop(e)
                            window.LAYER = this.options.callback.call(map.editTools);
                        }, this);
                        return container;
                    }
                });

                // Custom Control for adding Polygon Area Markers
                L.NewPolygonControl = L.EditControl.extend({
                    options: {
                        position: 'topleft',
                        callback: map.editTools.startPolygon,
                        kind: 'polygon',
                        html: '▰'
                    }

                });

                // Custom Control for adding Normal Markers
                L.NewMarkerControl = L.EditControl.extend({
                    options: {
                        position: 'topleft',
                        callback: function (LatLng) {
                            // Create a Marker with our custom Icon
                            map.editTools.startMarker(LatLng, {icon: idleIcon});
                        },
                        kind: 'marker',
                        html: '🖈'
                    }

                });

                // Custom Control for adding Normal Markers
                L.NewOverlayControl = L.EditControl.extend({
                    options: {
                        position: 'topleft',
                        callback: function () {
                            selectImage();
                        },
                        kind: 'marker',
                        html: '⧈'
                    }

                });

                // Add the Custom Controls to our map.
                map.addControl(new L.NewMarkerControl());
                map.addControl(new L.NewPolygonControl());
                map.addControl(new L.NewOverlayControl());
            }

            // Bind Map events
            map.on('editable:drawing:start', addTooltip);
            map.on('editable:drawing:end', removeTooltip);
            map.on('editable:drawing:click', updateTooltip);
            map.on('editable:drawing:cancel', cancelDrawing);
            map.on('editable:drawing:commit', commitDrawing);
            map.on('editable:enable', setFeatureActive);
            map.on('editable:disable', setFeatureInactive);

            // Check the Escape and Backspace keys for deselecting / removing layers.
            $(document).keydown(function (e) {
                let focused = document.activeElement;
                if (focused.isEqualNode(mainContainer[0]) || mainContainer[0].contains(focused)) {
                    if (e.key === "Escape") { // escape key maps to keycode `27`
                        if (drawingShape) {
                            map.editTools.stopDrawing();
                        } else {
                            selectFeature(null);
                        }
                    }
                    if (e.key === "Backspace" && !drawingShape) { // escape key maps to keycode `27`
                        let feature = currentActiveFeature;
                        if(feature !== null) {
                            selectFeature(null);
                            removeFeature(feature);
                        }

                        let overlay = currentSelectedOverlay;
                        if(overlay !== undefined && overlay !== null) {
                            deselectOverlay();
                            let id = -1;
                            for(let i = 0; i <overlays.length; i++) {
                                if(overlays[i].image === overlay) {
                                    id = i;
                                }
                            }
                            if(id !== -1) {
                                overlays.splice(id,1);
                                updateLayers();
                            }
                            overlay.remove();
                            updateLayerInfo();
                        }
                    }
                    if (e.key === "Enter" && firstVertexDrawn) {
                        console.log("ENTER!");
                        map.editTools.commitDrawing();
                    }
                }
            });

            let data = {
                'action': endpoint_get_geo_json.action,
                'nonce': endpoint_get_geo_json.nonce,
                'map_id': mapId,
            }

            selectFeature(null);

            // noinspection JSUnresolvedVariable
            $.post(endpoint_get_geo_json.url, data, function (response) {
                if(response !== "") {
                    let json = JSON.parse(response);
                    importGeoJson(json, defaultLayer, false);

                    //Deselect all layers at start.
                    selectFeature(null);

                    console.log("Initialized ManageE-Map");

                    closeMenu();
                }
            });
        }

        initialize();
    });


});

function showMeasurementNode(nodeId) {
    jQuery('.managee-map').trigger('showMeasurementNode',[nodeId]);
}

function showNodeById(nodeId) {
    jQuery('.managee-map').trigger('showNodeById',[nodeId]);
}