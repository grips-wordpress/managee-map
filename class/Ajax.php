<?php


namespace ManageEMap;
class Ajax
{
    /**
     * A dictionary of available endpoints. Used to print the ajax javascript information.
     * @var array
     */
    private static $endpoints = [];

    /**
     * @param $endpoint string the name of the hook.
     * @param $action callable
     * @param $admin
     */
    public static function addEndpoint($endpoint, $action , $admin) {
        $address = Settings::$prefix . '_' . $endpoint;
        $nonceId = "ajax_nonce_".$address;
        if(!$admin) {
            //Allow the hook to be called from anywhere!
            add_action('wp_ajax_nopriv_'.$address, function() use ($action, $nonceId) { self::checkAjaxRequest($nonceId); $action(); });
        }
        // valid for admins only
        add_action('wp_ajax_'.$address, function() use ($action, $nonceId) { self::checkAjaxRequest($nonceId); $action(); });
        self::$endpoints[$endpoint] = array("url"=>admin_url( 'admin-ajax.php' ), "action"=>$address, "nonceId"=>$nonceId);
    }

    /**
     * Checks if a request to the current ajax endpoint is valid and contains the correct nonce.
     * @param $nonceId string the id of the nonce the request must be valid for.
     */
    public static function checkAjaxRequest($nonceId) {
        $nonce = $_POST['nonce'];
        if ( ! wp_verify_nonce( $nonce, $nonceId) )
            wp_die ( 'Invalid Request!');
    }

    /**
     * Print the data of an endpoint for use in a javascript file. This also adds a new nonce to the data array to be
     * used for requests to this endpoint.
     * @param $endpoint string the name of the hook.
     * @return array the data required for the endpoint.
     */
    public static function getEndpointJavascriptData($endpoint) {
        require_once( ABSPATH . 'wp-includes/pluggable.php' );
        $data = self::$endpoints[$endpoint];
        $data['nonce'] = wp_create_nonce($data['nonceId']);
        return $data;
    }
}