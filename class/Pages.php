<?php

namespace ManageEMap;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

/**
 * Class Pages Controls loading of pages from the Pages folder.
 */

class Pages
{
    /**
     * @var string[] List of headers to load from page files using the wordpress function get_file_data()
     */
    private static $pageFileHeaders = array('type' => 'type', 'name'=>'name', 'slug'=>'slug', 'menu-name'=>'menu-name', 'icon'=>'icon', 'rights'=>'rights', 'template'=>'template', 'admin-bar'=>'admin-bar', 'hidden'=>'hidden');

    /**
     * Initialize all page hooks
     */
    public static function init() {
        add_action('admin_menu', Utilities::getFunctionPointer("setupAdminMenu"));
        add_action('admin_init', Utilities::getFunctionPointer("setupAdminMenuSeparator"));
        add_filter('the_posts', Utilities::getFunctionPointer("overrideFrontendPosts"), 9999);
        add_filter('page_template', Utilities::getFunctionPointer("overrideFrontendPageTemplate"), 9999 );
        add_action('admin_bar_menu', Utilities::getFunctionPointer("overrideFrontendAdminBar"), 9999);
        add_action('show_admin_bar', Utilities::getFunctionPointer("overrideFrontendAdminBarVisibility"), 9999);
    }

    /**
     * Hide the Admin bar completely if required.
     * @param $visible
     * @return bool
     */
    public static function overrideFrontendAdminBarVisibility($visible) {
        global $wp_query, $wp;
        $possibleFiles = self::findFilesOfType("frontend");
        foreach ($possibleFiles as $file) {
            if ($file['slug'] == $wp_query->query['pagename'] || $file['slug'] == $wp->request ) {
                if(strtolower($file['admin-bar']) == "false") {
                    return false;
                }
            }
        }
        return $visible;
    }

    /**
     * Override the Admin bar for custom pages.
     * @param $wp_admin_bar
     */
    public static function overrideFrontendAdminBar($wp_admin_bar) {
        global $wp_query, $wp;
        $possibleFiles = self::findFilesOfType("frontend");
        foreach ($possibleFiles as $file) {
            if ($file['slug'] == $wp_query->query['pagename'] || $file['slug'] == $wp->request ) {
                $wp_admin_bar->remove_node('edit');
                $adminPages = self::findFilesOfType("admin");
                if(count($adminPages) > 0) {
                    foreach ($adminPages as $adminPage) {
                        if(count(explode("/",$adminPage['slug'])) == 1) {
                            $wp_admin_bar->add_node(array(
                                'id'=>'view-plugin',
                                'title'=>Settings::$name,
                                'href'=>"/wp-admin/admin.php?page=".Settings::$name."-".$adminPage['slug']
                            ));
                            return;
                        }
                    }
                }
            }
        }
    }

    /**
     *
     * @param $page_template
     * @return string
     * @noinspection PhpUnused
     */
    public static function overrideFrontendPageTemplate( $page_template ) {
        global $wp_query, $wp;
        $possibleFiles = self::findFilesOfType("frontend");
        foreach ($possibleFiles as $file) {
            if ($file['slug'] == $wp_query->query['pagename'] || $file['slug'] == $wp->request ) {
                $template = strtolower($file['template']);
                if($template != "" && $template != "theme") {
                    $templateFile = Settings::$pluginDirectory . '/templates/' . $file['template'] . '.php';
                    if(file_exists($templateFile)) {
                        $page_template = $templateFile;
                    }
                }
            }
        }
        return $page_template;
    }

    /**
     * Allow posts to be overridden in the Frontend.
     * @param $posts
     * @return array
     */
    public static function overrideFrontendPosts($posts) {
        global $wp_query, $wp;
        $possibleFiles = self::findFilesOfType("frontend");
        foreach ($possibleFiles as $file) {
            if($file['slug'] == $wp_query->query['pagename'] || $file['slug'] == $wp->request ) {
                remove_action('template_redirect', 'redirect_canonical');
                $template = strtolower($file["template"]);
                $isBaseTheme = $template == "theme" || $template == "";
                $posts = null;
                $posts[] = self::create_fake_page($file['name'], $file['name'], self::getRenderedHTML($file['file']));
                $wp_query->is_page = true;
                $wp_query->is_singular = $isBaseTheme;
                //Longer permalink structures may not match the fake post slug and cause a 404 error so we catch the error here
                unset($wp_query->query["error"]);
                $wp_query->query_vars["error"] = "";
                $wp_query->is_404 = false;
                $wp_query->found_posts = 1;
                $wp_query->current_comment = 0;
                break;
            }
        }
        return $posts;
    }

    /**
     * Creates virtual fake post for rendering on a given template.
     * @param $pagename
     * @param $title
     * @param $content
     * @return stdClass
     */
    private static function create_fake_page( $pagename, $title, $content ) {
        $post                 = new \stdClass;
        $post->ID             = - 1;
        $post->post_author    = 1;
        $post->post_date      = current_time( 'mysql' );
        $post->post_date_gmt  = current_time( 'mysql', 1 );
        $post->post_content   = $content;
        $post->post_title     = $title;
        $post->post_excerpt   = "";
        $post->post_status    = 'publish';
        $post->comment_status = 'closed';
        $post->ping_status    = 'closed';
        $post->post_name      = $title;
        $post->post_type      = "custom";
        $post->guid           = get_bloginfo( 'wpurl' ) . '/' . $pagename;
        $post->filter         = 'raw';
        $post->comment_count  = 0;

        return $post;
    }

    /**
     * Render a php file to its html output.
     * @param $file
     * @return false|string
     */
    private static function getRenderedHTML($file) {
        ob_start();
        /** @noinspection PhpIncludeInspection */
        include($file);
        $renderedHtml = ob_get_contents();
        ob_end_clean();
        return $renderedHtml;
    }

    /**
     * Find all include files of the specified type
     * @param $type
     * @return array
     */
    private static function findFilesOfType($type) {
        $files = [];
        $directoryIterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(Settings::$pluginDirectory . '/pages'));
        foreach ($directoryIterator as $path) {
            $pathinfo = pathinfo($path);
            if ($pathinfo['extension'] == 'php') {
                $data = get_file_data($path, self::$pageFileHeaders);
                if(strtolower($data["type"]) == strtolower($type)) {
                    $data['file'] = $path;
                    $files[] = $data;
                }
            }
        }
        return $files;
    }

    /**
     * Create the Admin Menu from files in the /pages folder of the plugin. See the readme.txt in the folder for information
     * about required header information!
     * TODO Expensive! Use a cache and only parse files if the creation date changed from the last time it was loaded!
     */
    public static function setupAdminMenu() {
        $files = self::findFilesOfType("admin");
        $rootPage = new Page("","","","","");
        foreach ($files as $file) {
            if($file['name'] != "" && $file['slug'] != "") {
                //Valid File! Add it to the Menu Hierarchy.
                if(strtolower($file['hidden']) != "true") {
                    $rootPage->addChild($file['name'], $file['menu-name'], $file['slug'], $file['file'], $file['icon']);
                } else {
                    add_submenu_page(null,$file['name'], $file['name'], 'manage_options', Settings::$slugPrefix . "-" . $file['slug'], function() use($file) { Page::renderPage($file['file']); });
                }
            }
        }
        //Setup the Page links.
        $rootPage->register();
    }

    /**
     * Add a separator to the admin main menu after all pages of the Plugin where added!
     */
    function setupAdminMenuSeparator() {
        global $menu;
        $validItems = [];
        if (is_admin()) {
            foreach ((array)$menu as $key => $item) {
                //var_dump($item);
                //echo '<br><br>';
                if (strpos($item[2], Settings::$slugPrefix) !== false) {
                    $validItems[] = $key;
                }
            }
        }
        if(count($validItems) > 0) {
            array_splice($menu, $validItems[count($validItems)-1]+1, 0, array(array(
                0 => '',
                1 => 'read',
                2 => 'separator-last',
                3 => '',
                4 => 'wp-menu-separator'
            )));
        }
    }
}