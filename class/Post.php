<?php


namespace ManageEMap;

/**
 * Class Post used to create secure post requests and forms for these requests.
 */
class Post
{
    private static $postActions = [];

    /**
     * Register a new Post request.
     * @param string $name the name of the request to register.
     * @param callable $callback the function called when the post is validated and called.
     */
    public static function RegisterPostAction($name, $callback) {
        $action = Settings::$prefix . '_' . $name;
        self::$postActions[$name] = array("nonce_field"=>Settings::$prefix.$name."_nonce_field", "action_name"=>$action);
        add_action('admin_post_'.$action, function () use ($callback, $name) {
            if(self::ValidatePost($name)) {
                $callback();
            } else {
                wp_die("Invalid Request.");
            }
        });
    }

    /**
     * Internal function to validate post requests.
     * @param string $name the name of the post action to validate
     * @return bool true if the request is valid.
     */
    private static function ValidatePost($name) {
        if(!isset(self::$postActions[$name])) {
            return false;
        }
        if(!isset( $_POST[self::$postActions[$name]["nonce_field"]])) return false;
        if(!wp_verify_nonce( $_POST[self::$postActions[$name]["nonce_field"]], self::$postActions[$name]["action_name"])) return false;

        //Todo check if admin required and user is admin!

        return true;
    }

    /**
     * Print the required hidden post fields for a form
     * @param string $name the name of the registered post request.
     */
    public static function GetFormData($name) {
        if(!isset(self::$postActions[$name])) {
            wp_die("Unknown post request ".$name);
        }
        echo '<input type="hidden" name="action" value="'.self::$postActions[$name]["action_name"].'">';
        wp_nonce_field( self::$postActions[$name]["action_name"], self::$postActions[$name]["nonce_field"] );
    }
}