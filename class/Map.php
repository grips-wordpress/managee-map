<?php


namespace ManageEMap;

/**
 * Class Map Represents a managee-map object in the database.
 * @package ManageEMap
 */
class Map
{
    public $id;
    public $name;
    public $tileUrl;
    public $geoJson;

    /**
     * Map constructor.
     * @param $id
     * @param $name
     * @param $tileUrl
     * @param $geoJson
     */
    public function __construct($id, $name, $tileUrl, $geoJson)
    {
        $this->id = $id;
        $this->name = $name;
        $this->tileUrl = $tileUrl;
        $this->geoJson = $geoJson;
    }

    /**
     * Render the maps view depending on if the view is the editor or not.
     * @param bool $shortcode
     * @param string $height
     * @param bool $fullscreen
     * @return string
     */
    public function render($shortcode, $height, $fullscreen = false)
    {
        $isEditor = $shortcode == false;

        wp_enqueue_style('dashicons');
        ImportHelper::RequireStyle('leaflet');
        ImportHelper::RequireStyle('spectrum');
        ImportHelper::RequireStyle('map-handler');
        ImportHelper::RequireStyle('leaflet.toolbar');
        ImportHelper::RequireStyle('MarkerCluster');
        ImportHelper::RequireStyle('MarkerCluster.Default');
        ImportHelper::RequireStyle('leaflet.distortableimage');

        ImportHelper::RequireScript('leaflet');
        ImportHelper::RequireScript('spectrum');
        ImportHelper::RequireScript('leaflet.toolbar');
        ImportHelper::RequireScript('Path.Drag', [], ["leaflet"]);
        ImportHelper::RequireScript('L.Path.Transform', [], ["leaflet"]);
        ImportHelper::RequireScript('Leaflet.Editable', [], ["leaflet","Path.Drag"]);
        ImportHelper::RequireScript('leaflet.distortableimage', [], ["leaflet", "leaflet.toolbar"]);
        ImportHelper::RequireScript('leaflet.markercluster-src', [], ["leaflet"]);

        $variables = [];
        $variables["backend"] = array('root_url' => get_home_url(), 'is_editor'=>$isEditor);
        $variables["endpoint_get_geo_json"] = Ajax::getEndpointJavascriptData("get_map_geo_json");
        $variables["measurement_nodes"] = \ManageEConnector\Connector::getNodeList(true);

        if($isEditor) {
            $variables["endpoint_put_geo_json"] = Ajax::getEndpointJavascriptData("put_map_geo_json");
            $variables["endpoint_put_node_content"] = Ajax::getEndpointJavascriptData("put_node_content");
            $variables["endpoint_get_node_content"] = Ajax::getEndpointJavascriptData("get_node_content");
        }

        ImportHelper::RequireScript('map-handler', $variables, ["jquery", "leaflet", "spectrum"]);

        if($isEditor) {
            wp_enqueue_media();
        }

        $editor = "";
        if($isEditor) {
            $editor = 'data-editor="true"';
        }

        return '<div class="managee-map uninitialized" ' . $editor . ' id="map-'.$this->id.'" data-fullscreen="'.(($fullscreen && $shortcode)?"true":"false").'" data-map-id="' . $this->id . '" data-map-name="' . $this->name . '" data-map-height="' . $height . '" data-map-url="'.$this->tileUrl.'" style="width: 100%;"></div>';
    }

    /**
     * Update the maps data to the database.
     */
    public function update() {
        global $wpdb;
        $wpdb->update( $wpdb->prefix.Settings::$databaseTablePrefix."map",
            array(
                'name' => $this->name,
                'tile_url' => $this->tileUrl,
                'geo_json' => $this->geoJson,
            ),
            array("id"=>$this->id),
            array(
                '%s', '%s', '%s'
            )
        );
        $wpdb->update(
            $wpdb->prefix.Settings::$databaseTablePrefix."node_content",
            array('published'=>1),
            array('map_id' => $this->id)
        );
    }

    /**
     * Fetch a map with the given id.
     * @param $id
     * @return Map
     */
    public static function fetch($id) {
        global $wpdb;
        $row = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM ".$wpdb->prefix.Settings::$databaseTablePrefix."map"." WHERE id = ".intval($id)));
        return new Map($row->id, $row->name, $row->tile_url, $row->geo_json);
    }

    /**
     * Fetch all maps in the database.
     * @return array
     */
    public static function fetchAll() {
        global $wpdb;
        $results = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM ".$wpdb->prefix.Settings::$databaseTablePrefix."map"));
        $maps = [];
        foreach ($results as $row) {
            $maps[] = new Map($row->id, $row->name, $row->tile_url, $row->geo_json);
        }
        return $maps;
    }

    /**
     * Create a new Map and return it's id.
     * @return int The new Maps id.
     */
    public static function create() {
        global $wpdb;
        $wpdb->insert( $wpdb->prefix.Settings::$databaseTablePrefix."map",
            array(
                'id'=>null,
                'name' => "New Map",
                'tile_url' => "https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png",
                'geo_json' => "[]",
            ),
            array(
                '%s', '%s', '%s'
            )
        );
        return $wpdb->insert_id;
    }

    /**
     * Remove the map from the database.
     */
    public function remove()
    {
        global $wpdb;
        $wpdb->delete(
            $wpdb->prefix.Settings::$databaseTablePrefix."map",
            array(
                'id' => $this->id
            )
        );
    }
}