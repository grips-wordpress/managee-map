<?php

namespace ManageEMap;
/**
 * Class ImportHelper enqueues styles and scripts for the plugin.
 */
class ImportHelper
{
    /**
     * @param string $name the name of the script file to load from the /scripts directory.
     * @param array $localizations array of items that are replaced in the script file.
     * @param array $dependencies script names that this script depends on.
     * @param string $nameOverride override the name used to load this script. Used when script names are conflicting other plugins.
     */
    public static function RequireScript($name, $localizations = array(), $dependencies = array(), $nameOverride = "")
    {
        $name = str_replace(".js","",$name);
        $internalName = $nameOverride != ""?$nameOverride:$name;
        $path = "scripts/".$name.".js";
        $filePath = Settings::$pluginDirectory .'/'.$path;
        if(!file_exists($filePath)) {
            return;
        }
        $versionCode = date("ymd-Gis", filemtime($filePath));
        $publicPath = plugins_url($path, Settings::$pluginFile);
        wp_enqueue_script($internalName, $publicPath, $dependencies, $versionCode);
        foreach ($localizations as $key=>$localization) {
            wp_localize_script($internalName, $key, $localization);
        }
    }

    /**
     * @param string $name the name of the style file to load from the /styles directory.
     * @param array $dependencies style names that this style depends on.
     * @param string $nameOverride override the name used to load this style. Used when style names are conflicting other plugins.
     */
    public static function RequireStyle($name, $dependencies = array(), $nameOverride = "")
    {
        $name = str_replace(".css","",$name);
        $internalName = $nameOverride != ""?$nameOverride:$name;
        $path = "styles/".$name.".css";
        $filePath = Settings::$pluginDirectory .'/'.$path;
        if(!file_exists($filePath)) {
            return;
        }
        $versionCode = date("ymd-Gis", filemtime($filePath));
        $publicPath = plugins_url($path, Settings::$pluginFile);
        wp_enqueue_style($internalName, $publicPath, $dependencies, $versionCode);
    }
}