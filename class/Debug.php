<?php

namespace ManageEMap;
/**
 * Class Debug Read and Write Logs to the Plugins log database.
 */
class Debug
{
    public static function log($message) {
        global $wpdb;
        $wpdb->insert('wp_'.Settings::$databaseTablePrefix.'log', array('id' => null, 'date' => current_time('mysql'), 'message'=>$message));
    }

    public static function GetLogs($itemsPerPage = 0, $page = 0) {
        global $wpdb;
        $limit = intval($itemsPerPage);
        if($limit <= 0) {
            $limit = 100;
        }

        $offset = intval($page) * $itemsPerPage;
        if($offset <= 0) {
            $offset = 0;
        }

        $results = $wpdb->get_results(
            $wpdb->prepare("SELECT * FROM ".$wpdb->prefix . Settings::$databaseTablePrefix."log ORDER BY date DESC LIMIT %d OFFSET %d", $limit, $offset)
        );

        return $results;
    }
}