<?php

namespace ManageEMap;
/**
 * Class DatabaseHelper
 */
class Database
{
    /**
     * Check the Update file sql for new rows and run them. The update the internal update line in the database so that
     * the lines are not loaded again.
     * @param $file
     */
    public static function checkUpdates($file)
    {
        global $wpdb;

        $tablePrefix = $wpdb->prefix . Settings::$databaseTablePrefix;

        // Check if the Version Table Exists
        $versionTableExistsQuery = $wpdb->query('SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = "' . $tablePrefix . 'version"');

        if ($versionTableExistsQuery < 1) {
            // Create the Version Table!
            if (!$wpdb->query("CREATE TABLE " . $tablePrefix . "version (`current_row` INT(11) NOT NULL);")) {
                wp_die("Error: Could not create Version Table '" . $tablePrefix . "version'!");
            }
            if (!$wpdb->query("INSERT INTO " . $tablePrefix . "version (`current_row`) VALUES (0);")) {
                wp_die("Error: Could not write initial row value to Table '" . $tablePrefix . "version'!");
            }
        }

        // Read the Current Version from the Version Table.
        $versionTableQuery = $wpdb->get_results('SELECT * FROM ' . $tablePrefix . 'version');
        if ($versionTableQuery == false) {
            wp_die("Error: Could not read from Version Table '" . $tablePrefix . "version'!");
        }
        $currentInstalledRow = intval($versionTableQuery[0]->current_row);

        $queriesFile = str_replace("TABLE_PREFIX_", $tablePrefix, file_get_contents($file));

        $queries = explode(";\n", str_replace("\r\n", "\n", $queriesFile));
        $newInstalledRow = $currentInstalledRow;
        for ($i = $currentInstalledRow; $i < count($queries); $i++) {
            if ($queries[$i] == "") {
                if ($i != count($queries) - 1) {
                    $newInstalledRow++;
                    continue;
                } else {
                    break;
                }
            }
            $currentQuery = $queries[$i];
            if (substr($currentQuery, strlen($currentQuery) - 1, 1) != ";") {
                $currentQuery .= ';';
            }
            $wpdb->query($currentQuery);
            $newInstalledRow++;
        }
        $wpdb->query("UPDATE " . $tablePrefix . "version SET `current_row` = " . $newInstalledRow . ";");
    }
}