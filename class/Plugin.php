<?php

namespace ManageEMap;
/**
 * Class Plugin Wrapper class to initialize all Boilerplate Plugin functions with one call from the main Plugin file.
 */
class Plugin
{
    /**
     * Initialize all Plugin functionality.
     * @param string $file the path of the main plugin file.
     * @param string $gitlabUrl the gitlab url of the plugin used for updates
     */
    public static function init($file, $gitlabUrl) {
        // Initialize all settings variables
        Settings::init($file);
        // Initialize the Updater Scripts for Installing/Updating and Uninstalling the Plugin
        Updater::init($gitlabUrl);
        //Load all Pages of the Plugin.
        Pages::init();
    }
}