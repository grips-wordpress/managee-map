<?php

namespace ManageEMap;
class Shortcode
{
    /**
     * Register a shortcode.
     * TODO add more functionality to make certain tasks with shortcodes easier.
     * @param string $name the name of the shortcode.
     * @param callable $function the callback function used to draw the shortcode. Passes the $attributes parameter!
     */
    public static function register($name, $function) {
        add_shortcode($name, function($attributes) use ($function) {
            return $function($attributes);
        });
    }
}