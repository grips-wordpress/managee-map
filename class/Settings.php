<?php

namespace ManageEMap;
/**
 * Class Settings contains variables that are shared by all plugins scripts like the directory and name of the plugin.
 */
class Settings
{
    /**
     * @var string The Required connector version for this Plugins version.
     */
    public static $requiredConnectorVersion = '1.1.4';

    /**
     * @var string The Internal name of the plugin.
     */
    public static $name;

    /**
     * @var string The prefix after the wordpress database prefix for this plugins database tables.
     */
    public static $databaseTablePrefix;

    /**
     * @var string the main File of the Plugin
     */
    public static $pluginFile;

    /**
     * @var string the main directory of the Plugin
     */
    public static $pluginDirectory;

    /**
     * @var string the slug prefix for all pages of this plugin.
     */
    public static $slugPrefix;

    /**
     * @var string the prefix for all functions of this plugin.
     */
    public static $prefix;

    /**
     * @var string the version number of the plugin
     */
    public static $version;

    /**
     * Initialize all required Variables
     * @param $pluginFilePath string the file location of the plugin
     */
    public static function init($pluginFilePath) {
        self::$pluginFile = $pluginFilePath;
        self::$pluginDirectory = dirname(self::$pluginFile);
        self::$name = basename($pluginFilePath,'.php');
        self::$prefix = str_replace("-","_",self::$name);
        self::$slugPrefix = str_replace("_","-",self::$name);
        self::$databaseTablePrefix = str_replace("-","_",self::$name)."_";
        $plugin_data = get_file_data(self::$pluginFile, array('Version' => 'Version'), false);
        self::$version = $plugin_data['Version'];
    }
}