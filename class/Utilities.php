<?php

namespace ManageEMap;
class Utilities
{
    /**
     * Get a pointer to an internal class function. This is used to link hooks to static class functions.
     * @param $functionName
     * @return array
     */
    public static function getFunctionPointer($functionName)
    {
        return array(debug_backtrace()[1]["class"], $functionName);
    }

    /**
     * Check if a string ends with another string
     * @param string $haystack the string to search in
     * @param string $needle the ending to search for
     * @return bool true if $haystack ends with $needle
     */
    public static function stringEndsWith( $haystack, $needle ) {
        $length = strlen( $needle );
        if( !$length ) {
            return true;
        }
        return substr( $haystack, -$length ) === $needle;
    }
}