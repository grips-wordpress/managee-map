<?php

namespace ManageEMap;
/**
 * Class Page describes a page in the Backend. Can eb used to dynamically build the wp_admin Hierarchy of the sidebar.
 */
class Page
{
    private $name;
    private $menuName;
    private $slug;
    private $file;
    private $icon;
    /**
     * @var Page[] The child Pages of this page.
     */
    private $children;

    /**
     * Page constructor.
     * @param string $name name of the page displayed in the sidebar under its parent.
     * @param string $menuName top level name of the page for the sidebar. Only for parent Pages!
     * @param string $slug slug of the admin page. If it contains a "/" it is a subdirectory!
     * @param string $file file path for the page
     * @param string $icon icon used if this is a toplevel entry
     */
    public function __construct($name, $menuName, $slug, $file, $icon)
    {
        $this->name = $name;
        $this->menuName = $menuName;
        $this->slug = $slug;
        $this->file = $file;
        $this->icon = $icon;
        $this->children = [];
    }

    /**
     * Add a child to this page.
     * @param string $name name of the child page displayed in the sidebar under its parent.
     * @param string $menuName top level name of the child for the sidebar. Only for parent Pages!
     * @param string $slug slug of the child page. If it contains a "/" it is a subdirectory!
     * @param string $file file path for the child page
     * @param string $icon icon used if the child is a toplevel entry
     */
    public function addChild($name, $menuName, $slug, $file, $icon) {
        $slugSplit = explode("/",$slug);
        if(count($slugSplit) > 1) {
            $currentSlug = $slugSplit[0];
            unset($slugSplit[0]);
            $nextSlug = join("/",$slugSplit);

            if(!isset($this->children[$currentSlug])) {
                $this->children[$currentSlug] = new Page($currentSlug, $currentSlug, $currentSlug, "", $icon);
            }
            $this->children[$currentSlug]->addChild($name, $menuName, $nextSlug, $file, $icon);
        } else {
            if(!isset($this->children[$slug])) {
                $this->children[$slug] = new Page($name, $menuName, $slug, $file, $icon);
            } else {
                if($file != "") {
                    $this->children[$slug]->file = $file;
                    $this->children[$slug]->name = $name;
                    $this->children[$slug]->menuname = $menuName;
                    $this->children[$slug]->icon = $icon;
                }
            }
        }
    }

    /**
     * Register the Page and its children with wordpress recursively.
     * @param string $slug the current path of the recursive process.
     */
    public function register($slug = "") {
        if($this->slug != "") {
            if($this->menuName == "") {
                $this->menuName = $this->name;
            }
            if($slug == "") {
                add_menu_page($this->name, $this->name, 'manage_options', Settings::$slugPrefix . "-" . $this->slug, "", $this->icon);
                add_submenu_page(Settings::$slugPrefix . "-" . $this->slug, $this->name, $this->menuName, 'manage_options', Settings::$slugPrefix . "-" . $this->slug, function() { self::renderPage($this->file); });
            } else {
                add_submenu_page(Settings::$slugPrefix . "-" . $slug, $this->name, $this->name, 'manage_options', Settings::$slugPrefix . "-" . $this->slug, function() { self::renderPage($this->file); });
            }
            if($slug != "") {
                $slug .= "/";
            }
            $slug .= $this->slug;
        }
        foreach ($this->children as $key=>$child) {
            $child->register($slug);
        }
    }

    /**
     * Render a page given its path.
     * @param string $pageFilePath
     */
    public static function renderPage($pageFilePath) {
        echo '<div class="wrap">';
        /** @noinspection PhpIncludeInspection */
        include($pageFilePath);
        echo '</div>';
    }
}