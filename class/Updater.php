<?php

namespace ManageEMap;
use Puc_v4_Factory;

/**
 * Class Updater Handles all update/install/uninstall processes of the plugin.
 */
class Updater
{
    /**
     * Initialize the updater connection with the gitlab repository.
     * @param string $gitlabUrl the gitlab url of the project
     */
    public static function init($gitlabUrl) {
        /** @noinspection PhpIncludeInspection */
        require Settings::$pluginDirectory . '/plugin-update-checker/plugin-update-checker.php';
        if ( !is_multisite() || is_network_admin() ) {
            Puc_v4_Factory::buildUpdateChecker(
                $gitlabUrl,
                Settings::$pluginFile,
                Settings::$name
            );
        }
        self::initHooks();
    }


    /**
     * Initialize all required Wordpress Hooks.
     */
    private static function initHooks() {
        //Check if the Plugin was uninstalled.
        register_uninstall_hook(Settings::$pluginFile, Utilities::getFunctionPointer('pluginUninstalled'));
        //Check for Installation Hooks and Updates of the Plugin.
        register_activation_hook(Settings::$pluginFile, Utilities::getFunctionPointer('pluginActivated') );
        register_deactivation_hook(Settings::$pluginFile, Utilities::getFunctionPointer('pluginDeactivated') );
        add_action( 'upgrader_process_complete', Utilities::getFunctionPointer('pluginUpdateCompleted'), 10, 2 );
        add_action( 'admin_notices', Utilities::getFunctionPointer('checkNotices') );
    }

    /**
     * This function runs when WordPress completes its upgrade process
     * It iterates through each plugin updated to see if ours is included
     * @param $upgrader_object array
     * @param $options array
     */
    public static function pluginUpdateCompleted( $upgrader_object, $options ) {
        // The path to our plugin's main file
        $our_plugin = plugin_basename( __FILE__ );
        // If an update has taken place and the updated type is plugins and the plugins element exists
        if( $options['action'] == 'update' && $options['type'] == 'plugin' && isset( $options['plugins'] ) ) {
            // Iterate through the plugins being updated and check if ours is there
            foreach( $options['plugins'] as $plugin ) {
                if( $plugin == $our_plugin ) {
                    // Run our Update Scripts.
                    set_transient( 'wp_' . Settings::$databaseTablePrefix . '_updated', 1);
                }
            }
        }
    }

    /**
     * Show a notice to anyone who has just updated this plugin
     * This notice shouldn't display to anyone who has just installed the plugin for the first time
     */
    public static function checkNotices() {
        // Check the transient to see if we've just updated the plugin
        if( get_transient( 'wp_' . Settings::$databaseTablePrefix . '_updated') ) {
            self::runUpdates();
            //echo '<div class="notice notice-success">'.Settings::$name.' - Datenbank wurde nach einem Update erfolgreich Aktualisiert.</div>';
            Debug::log("Plugin Updated.");
            delete_transient( 'wp_' . Settings::$databaseTablePrefix . '_updated');
        }

        // Check the transient to see if we've just activated the plugin
        if( get_transient( 'wp_' . Settings::$databaseTablePrefix . '_activated') ) {
            self::runUpdates();
            //echo '<div class="notice notice-success">'.Settings::$name.' - Plugin wurde erfolgreich Initialisiert.</div>';
            Debug::log("Plugin Activated and Updated.");
            // Delete the transient so we don't keep displaying the activation message
            delete_transient( 'wp_' . Settings::$databaseTablePrefix . '_activated');
        }
    }

    /**
     * Run this on activation
     * Set a transient so that we know we've just activated the plugin
     */
    public static function pluginActivated() {
        set_transient( 'wp_' . Settings::$databaseTablePrefix . '_activated', 1 );
    }

    /**
     * Run this on Deactivation
     */
    public static function pluginDeactivated() {
        Debug::log("Plugin Deactivated");
    }

    /**
     * Run this on Uninstall
     */
    public static function pluginUninstalled() {
        Debug::log("Plugin Uninstalled");
    }

    /**
     * Run the current sql updates in the /updates/database.sql file. This is called on Install/Activate and Updates.
     */
    public static function runUpdates() {
        $file = Settings::$pluginDirectory . '/updates/database.sql';
        Database::checkUpdates($file);
    }
}