<?php


namespace ManageEMap;
use ManageEConnector;
class Connector
{
    public static function versionValid() {
        $connectorRoot = WP_PLUGIN_DIR . '/managee-connector/managee-connector.php';
        $version = get_file_data($connectorRoot, array("version"=>"version"))["version"];
        if(version_compare($version,Settings::$requiredConnectorVersion) < 0) {
            return false;
        } else {
            return true;
        }
    }

    public static function connectorExists() {
        $connectorRoot = WP_PLUGIN_DIR . '/managee-connector/managee-connector.php';
        return file_exists($connectorRoot);
    }

    public static function connectorActive() {
        return is_plugin_active("managee-connector/managee-connector.php");
    }

    public static function isConnected() {
        return self::connectorExists() && self::versionValid() && self::connectorActive();
    }

    public static function requireConnector() {
        if(!self::connectorExists()) {
            wp_die('<div class="error notice"><p>This Plugin requires the ManageE Connector. You can obtain it from <a href="https://gitlab.com/grips-wordpress/managee-connector/-/archive/master/managee-connector-master.zip">here</a></p></div>.');
        }

        if(!self::versionValid()) {
            wp_die('<div class="error notice"><p>This Plugin requires the ManageE Connector v'.Settings::$requiredConnectorVersion.' to be Installed. Check for Updates on the Plugins page!</p></div>');
        }

        if(!self::connectorActive()) {
            wp_die('<div class="error notice"><p>The ManageE Connector plugin is disabled. Please enable it on the Plugins page!</p></div>');
        }
    }

    public static function GetSetting($name) {
        return ManageEConnector\Connector::GetSetting($name);
    }
}