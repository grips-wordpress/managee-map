<?php
/*
 * type: admin
 * name: Map
 * icon: dashicons-location-alt
 * slug: map
 * hidden: true
 */

namespace ManageEMap;
global $wpdb;

if (!defined('WPINC')) die;

$map = Map::fetch(intval($_GET['id']));


if ($map->id == "") {
    echo '<h1>Could not find Map with id ' . intval($_GET['id']) . '</h1>';
} else {
    echo '<style>#wpfooter { display: none !important; }</style>';
    echo '<h1>Edit Map "' . $map->name . '"</h1>';

    echo $map->render(false, "70vh");

//ManageE_Map_Plugin::createLocationSelect();
    echo '<div class="managee-map-popup" id="contentPopup"><div class="content"><div class="header"><span class="dashicons dashicons-no-alt cancel-button" style="cursor: pointer; font-size: 30px; margin-bottom: 10px;"></span></div>';
    echo '<h3>Edit Info Content</h3>';
    wp_editor("", "mangee_map_marker_editor");
    echo '<div style="text-align: right;">';
    echo '<button class="button button-primary save-button" style="margin-top: 10px; margin-right: 10px;"><i class="dashicons dashicons-yes-alt" style="vertical-align: middle; margin-bottom: 4px;"></i> Save</a>';
    echo '<button class="button button-primary cancel-button" style="margin-top: 10px;"><i class="dashicons dashicons-no-alt" style="vertical-align: middle; margin-bottom: 4px;"></i> Cancel</a>';
    echo '</div></div></div>';

    echo '<div class="managee-map-popup" id="jsonPopup"><div class="content"><div class="header"><span class="dashicons dashicons-no-alt cancel-button" style="cursor: pointer; font-size: 30px; margin-bottom: 10px;"></span></div>';
    echo '<h3>Import GeoJSON</h3><label>Layer</label><div class="layer-select-container"></div>';
    echo '<label>GeoJSON</label><textarea style="width: 100%; height: 200px;" class="geoJsonField"></textarea>';
    echo '<div style="text-align: right;">';
    echo '<button class="button button-primary save-button" style="margin-top: 10px; margin-right: 10px;"><i class="dashicons dashicons-yes-alt" style="vertical-align: middle; margin-bottom: 4px;"></i> Import GeoJson</a>';
    echo '<button class="button button-primary cancel-button" style="margin-top: 10px;"><i class="dashicons dashicons-no-alt" style="vertical-align: middle; margin-bottom: 4px;"></i> Cancel</a>';
    echo '</div></div></div>';
}
echo '</div><!-- End wrap -->';