<?php
/*
 * type: admin
 * name: ME-Maps
 * menu-name: ManageE Maps
 * icon: dashicons-location-alt
 * slug: settings
 */
namespace ManageEMap;
global $wpdb;

if (!defined('WPINC')) die;
?>
<h1>
    Managee Maps
</h1>
<p>
    Version: <?php echo Settings::$version; ?>
</p>
<?php Connector::requireConnector(); ?>
<?php
// this will get the data from your table
$maps = Map::fetchAll();
?>
<form method="post" action="<?php echo esc_html(admin_url('admin-post.php')); ?>">
    <?php Post::GetFormData("add_map"); ?>
    <button type="submit" class="button button-secondary" style="margin-bottom: 1rem;"><i class="dashicons dashicons-plus" style="vertical-align: middle;"></i> Neue Map Erstellen</button>
</form>
<table class="wp-list-table widefat fixed striped">
    <thead>
    <tr>
        <th>
            Id
        </th>
        <th>
            Name
        </th>
        <th>
            Embed Shortcode<br>
            Fullscreen Url
        </th>
        <th>
            Bearbeiten
        </th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($maps as $map) {
        ?>
        <tr>
            <td>
                <?php echo $map->id; ?>
            </td>
            <td>
                <?php echo $map->name; ?>
            </td>
            <td>
                [managee_map id=<?php echo $map->id; ?>]<br><br>
                <a href="<?php echo get_site_url(); ?>/managee-maps/?id=<?php echo $map->id; ?>" target="_blank"><i class="dashicons dashicons-desktop"></i> <?php echo get_site_url(); ?>/managee-maps/?id=<?php echo $map->id; ?></a>
            </td>
            <td>
                <a class="button button-primary" style="width: 100%; text-align: center; margin-bottom: 4px;" href="admin.php?page=managee-map-map&id=<?php echo $map->id; ?>"><i class="dashicons dashicons-edit" style="vertical-align: middle; margin-bottom: 4px;"></i> Bearbeiten</a>

                <form method="post" action="<?php echo esc_html(admin_url('admin-post.php')); ?>">
                    <?php Post::GetFormData("remove_map"); ?>
                    <input type="hidden" name="id" value="<?php echo $map->id; ?>">
                    <button type="submit" style="width: 100%; text-align: center;" class="button button-secondary"><i class="dashicons dashicons-trash" style="vertical-align: middle; margin-bottom: 4px;"></i> Löschen</button>
                </form>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>