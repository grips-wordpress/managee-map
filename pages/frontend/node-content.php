<?php
/*
 * This view renders a nodes content to a blank html canvas to be displayed in the node content view.
 * TODO: Add security to allow only calls when the actual parents map is visible.
 * type: frontend
 * template: blank
 * name: Node Content
 * admin-bar: false
 * slug: managee-maps/node-content
 */
namespace ManageEMap;
if (!defined('WPINC')) die;
global $wpdb;
$mapId = intval($_GET['map_id']);
$nodeId = intval($_GET['node_id']);
$latest = $_GET['latest'] == 'true';
?>

<?php
ImportHelper::RequireStyle('node-content');
wp_enqueue_style('dashicons');
if($latest) {
    $retrieve_data = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix.Settings::$databaseTablePrefix."node_content" . " WHERE `map_id` = $mapId AND `node_id` = $nodeId ORDER BY `edit_date` DESC LIMIT 1;");
} else {
    $retrieve_data = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix.Settings::$databaseTablePrefix."node_content" . " WHERE `published` = 1 AND `map_id` = $mapId AND `node_id` = $nodeId ORDER BY `edit_date` DESC LIMIT 1;");
}

$measurementNode = intval($_GET['measurement_node']);
$nodes = \ManageEConnector\Connector::getNodeList(false);
echo '<div class="managee-map-node-content">';
if(isset($nodes[$measurementNode])) {
    $node = $nodes[$measurementNode];
    if($node["parentId"] != -1 && isset($nodes[$node["parentId"]])) {
        $parentNode = $nodes[$node["parentId"]];
        echo '<div class="measurement-node-display" onclick="parent.showMeasurementNode('.$parentNode['id'].');">'.$parentNode["name"].'</div>';
        echo '<div class="next-icon"><span class="dashicons dashicons-arrow-down-alt"></span></div>';
    }
    echo '<div class="measurement-node-display-current">'.$nodes[$measurementNode]["name"].'</div>';

    $childNodes = [];
    foreach ($node['children'] as $childId) {
        if(isset($nodes[$childId])) {
            $childNodes[] = $nodes[$childId];
        }
    }

    if(count($childNodes) > 0) {
        echo '<div class="next-icon"><span class="dashicons dashicons-arrow-down-alt"></span></div>';
        echo '<div class="measurement-node-display">';
        foreach ($childNodes as $child) {
            echo '<p class="child" onclick="parent.showMeasurementNode('.$child['id'].');">' . $child['name'] . '</p>';
        }
        echo '</div>';
    }
    echo '<br>';
    //var_dump($nodes[$measurementNode]);
}


$content = '';
if(count($retrieve_data) == 1) {
    $content = ($retrieve_data[0])->content;
}
//$content = "Test 12345!";
echo $content;
echo '</div>';