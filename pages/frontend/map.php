<?php
/*
 * type: frontend
 * template: blank
 * name: ManageE Map
 * admin-bar: false
 * slug: managee-maps
 */
namespace ManageEMap;
if (!defined('WPINC')) die;
global $wpdb;
$mapId = intval($_GET['id']);
?>
<style>
    body{
        margin: 0 0 !important;
    }
</style>
[managee_map id="<?php echo $mapId; ?>" fullscreen="true"]