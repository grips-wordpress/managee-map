Pages
=====

You can add new Pages to the Plugin by adding files to this directory or any of its subdirectories.
Each File must then contain information in a php comment with the minimum set of parameters as follows:

```php
<?php
/*
 * name=Name of the Page
 * type=Page Type ('admin' or 'frontend')
 * slug=The path of the page in the frontend/backend (e.g. plugin-name/page-name)
 */
echo 'Page Content php / html';
?>
```

Admin Pages
-----------

For Admin pages the values icon and menu-name can also be set to display an icon and different name for Title/Subpage
using MenuName on parent slugs.

***Example:***

Creates the following Output in the Sidebar:

- ...
- (Spacer)
- Plugin => plugin-slug/settings
  - Home => plugin-slug/settings
  - Sub Page => plugin-slug/settings/subpage
- (Spacer)
- ...

main.php:

```php
<?php
/*
 * type: admin
 * name: Plugin
 * menu-name: Home
 * icon: dashicons-admin-plugins
 * slug: settings
 */
echo 'Page Content php / html';
?>
```

subpage.php

```php
<?php
/*
 * type: admin
 * name: Sub Page
 * slug: settings/subpage
 */
echo 'Page Content php / html';
?>
```

Frontend Pages
--------------

You can add a new frontend page by defining the page type as "frontend". The theme of the frontend page defaults to the
current themes template. If you want to use the blank template or a custom template file you can specify the
template parameter. (See /template/readme.txt for more information.)

***Example:***

frontendTheme.php
```php
<?php
/*
 * type: frontend
 * name: Frontend Page (Theme Template)
 * template: theme
 * slug: plugin-theme
 */
echo 'Page Content php / html';
?>
```

frontendBlank.php
```php
<?php
/*
 * type: frontend
 * name: Frontend Page (Blank Template)
 * template: blank
 * slug: plugin-blank
 */
echo 'Page Content php / html';
?>
```

###Admin Bar

In frontend pages, you can force hide the admin bar on certain pages using the admin-bar: false keyword.

***Example:***

hidden-admin.php
```php
<?php
/*
 * type: frontend
 * name: Page without admin bar
 * slug: noadmin
 * admin-bar: false
 */
echo 'Page Content php / html';
?>
```