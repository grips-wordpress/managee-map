=== ManageE Map ===
Contributors: GriPS Automation GmbH
Donate link: https://managee.de
Tags: utility
Requires at least: 5.0
Tested up to: 5.5.3
Stable tag: 4.3
Requires PHP: 7.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

ManageE Map Plugin for generating leaflet maps with nodes of different shapes.

== Description ==

ManageE Map Plugin for generating leaflet maps with nodes of different shapes. The nodes content can be edited with a shortcode-enabled editor so that they can display data from manageE Devices using shortcode plugins.

== Changelog ==

= 1.1.8 =
* Improve search

= 1.1.7 =
* Remove Search button until it is implemented
* Add node-content.css to the plugins /styles folder. This style is used for styling the content in the node content iframes.
* Add Alphabetic sorting to node layers that also checks for number sequences like node-1 and node-10 and sorts them correctly numerically!

= 1.1.6 =
* Add custom node links

= 1.1.5 =
* Fix node page rendering issues

= 1.1.4 =
* Add inter-node navigation and parent/child views to measurement nodes.

= 1.1.3 =
* Remove update information on Multisite Plugin page in Non-Network view.

= 1.1.2 =
* Add node child information

= 1.1.0 =
* Update to new Plugin Boilerplate.

== Upgrade Notice ==

= 1.1.0 =
* New versions can now be installed directly via the wordpress updater.
