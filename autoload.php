<?php

namespace manageemap {
    spl_autoload_register(function ($class_name) {
        $class_name = str_replace("ManageEMap\\","",$class_name);
        if (file_exists(__dir__ . '/class/' . $class_name . '.php')) {
            /** @noinspection PhpIncludeInspection */
            include __dir__ . '/class/' . $class_name . '.php';
        }
    });
}