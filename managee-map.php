<?php
/*
Plugin Name: ManageE Map
Plugin URI: https://managee.de
Description: ManageE Map Plugin for generating leaflet maps with nodes of different shapes. The nodes content can be edited with a shortcode-enabled editor so that they can display data from manageE Devices using shortcode plugins.
Author: Florian Bayer - GriPS Automation GmbH
Version: 1.1.8
Author URI: https://grips-automation.com
*/

namespace ManageEMap;

if ( !class_exists( 'ManageeMapPlugin' ) ) {

    require_once 'autoload.php';

    class ManageeMapPlugin
    {
        /**
         * Entry point of the Plugin called at the Bottom of this script.
         */
        public static function init()
        {
            // Initialize the Plugin
            Plugin::init(__FILE__, 'https://gitlab.com/grips-wordpress/managee-map/');
            Post::RegisterPostAction('add_map', Utilities::getFunctionPointer('addMap'));
            Post::RegisterPostAction('remove_map', Utilities::getFunctionPointer('removeMap'));
            Ajax::addEndpoint("get_map_geo_json", function() { self::getMapGeoJson(); }, false);
            Ajax::addEndpoint("put_map_geo_json", function() { self::putMapGeoJson(); }, true);
            Ajax::addEndpoint("get_node_content", function() { self::getNodeContent(); }, false);
            Ajax::addEndpoint("put_node_content", function() { self::putNodeContent(); }, true);

            Shortcode::register("managee_map", function($attributes) {
                $height = $attributes['height'];
                $allowedEndings = ["vh", "vw", "em", "px"];
                if(is_numeric($height)) {
                    $height = ($height*1) . 'px';
                } else {
                    $ok = false;
                    foreach ($allowedEndings as $allowedEnding) {
                        if(Utilities::stringEndsWith($height, $allowedEnding)) {
                            $ok = true;
                            break;
                        }
                    }
                    if(!$ok) {
                        $height = '300px';
                    }
                }
                if(isset($attributes['id'])) {
                    $map = Map::fetch($attributes['id']);
                    if($map->id != "") {
                        return $map->render(true, $height, isset($attributes['fullscreen']));
                    } else {
                        return self::failToRenderMap('invalid map-id was given in the shortcode!');
                    }
                } else {
                    return self::failToRenderMap('no map-id was given in the shortcode!');
                }
            });
        }

        /**
         * @param $reason
         * @return string the output for a map shortcode if it is invalid.
         */
        private static function failToRenderMap($reason) {
            return '<strong style="color: red">Could not render Managee Map:</strong> '.$reason.'';
        }

        /**
         * Create a new Map and forward to the edit Map view of the new Map.
         * @noinspection PhpUnused
         */
        public static function addMap() {
            $id = Map::create();
            header('Location: '.admin_url('admin.php').'?page=managee-map-map&id=' . $id);
        }

        /**
         * Remove a Map.
         * @noinspection PhpUnused
         */
        public static function removeMap() {
            $id = intval($_POST['id']);
            $map = Map::fetch($id);
            if($map->id != "") {
                $map->remove();
            }
            header('Location: '.admin_url('admin.php').'?page=managee-map-settings');
        }

        private static function getMapGeoJson() {
            $mapId = intval($_POST['map_id']);

            $map = Map::fetch($mapId);
            echo $map->geoJson;
            //End execution for custom output
            wp_die();
        }

        private static function putMapGeoJson() {
            global $wpdb;
            //TODO
            /*if(!self::canEdit()) {
                echo 'You do not have permission to access this page.';
                return;
            }*/

            $map = Map::fetch(intval($_POST['map_id']));
            if($map->id=="") {
                wp_die("Invalid Map Id!");
            } else {
                $map->tileUrl = $_POST['map_tile_url'];
                $map->name = $_POST['map_name'];
                $map->geoJson = json_encode($_POST['geo_json']);
                $map->update();

                //TODO make this nicer!
                if(isset($_POST['edited_nodes']) && is_array($_POST['edited_nodes'])) {
                    foreach($_POST['edited_nodes'] as $editedId) {
                        $wpdb->update(
                            $wpdb->prefix.Settings::$databaseTablePrefix."node_content",
                            array('published' => 1),
                            array('map_id' => $map->id, 'node_id' => $editedId)
                        );
                    }
                }
            }
            wp_die();
        }

        private static function getNodeContent() {
            //TODO
            /*if(!self::canEdit()) {
                echo 'You do not have permission to access this page.';
                return;
            }*/
            global $wpdb;
            $mapId = $_POST['map_id'];
            $nodeId = $_POST['node_id'];

            $retrieve_data = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix.Settings::$databaseTablePrefix."node_content" . " WHERE `map_id` = $mapId AND `node_id` = $nodeId ORDER BY `edit_date` DESC LIMIT 1;");

            if(count($retrieve_data) == 1) {
                $content = ($retrieve_data[0])->content;
                echo $content;
            }
            //End execution for custom output
            wp_die();
        }

        private static function putNodeContent() {
            //TODO
            /*if(!self::canEdit()) {
                echo 'You do not have permission to access this page.';
                return;
            }*/

            global $wpdb;
            //TODO add security!
            $wpdb->replace(
                $wpdb->prefix.Settings::$databaseTablePrefix."node_content",
                array(
                    'map_id' => $_POST['mapId'],
                    'node_id' => $_POST['nodeId'],
                    'content' => str_replace("\\\"", "\"", $_POST['content']),
                    'edit_date' => current_time('mysql'),
                    'published' => 0
                )
            );

            echo str_replace("\\\"", "\"", $_POST['content']);
            wp_die();
        }
    }

    ManageeMapPlugin::init();
}
